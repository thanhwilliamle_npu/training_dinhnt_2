﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using UnityEngine;
using Vector2 = UnityEngine.Vector2;

// ax + by + c = 0;
struct Line2D
{
    float a;
    float b;
    float c;

    public static Line2D CreateLine2D(Vector2 p1, Vector2 p2)
    {
        Line2D line;
        line.a = p1.y - p2.y;
        line.b = p2.x - p1.x;
        line.c = p1.x * p2.y - p2.x * p1.y;
        return line;
    }

    public static Line2D CreateLine2DPerpendicular(Vector2 point, Line2D pLine)
    {
        Line2D line;
        line.a = pLine.b;
        line.b = - pLine.a;
        line.c = - (line.a * point.x + line.b * point.y);
        return line;
    }

    private static EquationSerie2 _CreateEqS2FromLines(Line2D line1, Line2D line2)
    {
        return EquationSerie2.CreateEquationSerie2(
            line1.a, line1.b, - line1.c,
            line2.a, line2.b, -line2.c
        );
    }
    public static bool IsOverlap(Line2D line1, Line2D line2)
    {
        EquationSerie2 eq = _CreateEqS2FromLines(line1, line2);
        return MathUtils.AnalyzeSolutionForSerie2(eq) == NumberOfSolutionForSerie.INFINITE;
    }
    
    public static bool IsParallel(Line2D line1, Line2D line2)
    {
        EquationSerie2 eq = _CreateEqS2FromLines(line1, line2);
        return MathUtils.AnalyzeSolutionForSerie2(eq) == NumberOfSolutionForSerie.NONE;
    }
    
    public static bool IsIntersect(Line2D line1, Line2D line2)
    {
        EquationSerie2 eq = _CreateEqS2FromLines(line1, line2);
        return MathUtils.AnalyzeSolutionForSerie2(eq) == NumberOfSolutionForSerie.UNIQUE;
    }

    public static Vector2? IntersectLine(Line2D line1, Line2D line2)
    {
        if (!IsIntersect(line1, line2)) return null;
        
        float D = line1.a * line2.b - line2.a * line1.b;
        float Dx = - line1.c * line2.b + line2.c * line1.b;
        float Dy = - line1.a * line2.c + line2.a * line1.c;
        float x = Dx / D;
        float y = Dy / D;
        return new Vector2(x, y);
    }

    public static float DistanceBetween2Points(Vector2 point1, Vector2 point2)
    {
        return (point1 - point2).magnitude;
    }

    public static bool PointOnSegment(Vector2 point, Vector2 sa, Vector2 sb)
    {
        float AB = DistanceBetween2Points(sa, sb);
        float AP = DistanceBetween2Points(sa, point);
        float PB = DistanceBetween2Points(sb, point);
        return MathUtils.FloatEqual(AB, AP + PB);
    }
}

public static class MathTest_2D_Questions
{
    #region Closest point
    public static Vector2 ClosestPointOnLine(Vector2 p, Vector2 linePoint, Vector2 lineDirection)
    {
        if (MathUtils.FloatEqual(lineDirection.magnitude, 0)) return linePoint;
        Vector2 linePoint2 = linePoint + lineDirection;
        Line2D line = Line2D.CreateLine2D(linePoint, linePoint2);
        Line2D perpendicularLine = Line2D.CreateLine2DPerpendicular(p, line);
        return Line2D.IntersectLine(line, perpendicularLine).Value;
    }

    public static Vector2 ClosestPointOnSegment(Vector2 p, Vector2 sa, Vector2 sb)
    {
        Vector2 closestPointOnLine = ClosestPointOnLine(p, sa, sa - sb);
        if (Line2D.PointOnSegment(closestPointOnLine, sa, sb)) return closestPointOnLine;

        float distanceAtoP = Line2D.DistanceBetween2Points(p, sa);
        float distanceBtoP = Line2D.DistanceBetween2Points(p, sb);
        if (distanceAtoP < distanceBtoP) return sa;
        return sb;
    }
    #endregion

    #region Intersections
    public static void LineSegmentIntersect(Vector2 p, Vector2 d, Vector2 sa, Vector2 sb, out Vector2? intersect, out bool overlap)
    {
        Vector2 linePoint2 = p + d;
        Line2D line1 = Line2D.CreateLine2D(p, linePoint2);
        Line2D line2 = Line2D.CreateLine2D(sa, sb);
        Vector2? lineIntersect = Line2D.IntersectLine(line1, line2);
        if (lineIntersect == null) intersect = null;
        else if (!Line2D.PointOnSegment(lineIntersect.Value, sa, sb)) intersect = null;
        else intersect = lineIntersect;
        overlap = Line2D.IsOverlap(line1, line2);
    }

    public static void SegmentsIntersect(Vector2 s1a, Vector2 s1b, Vector2 s2a, Vector2 s2b, out Vector2? intersect, out bool overlap)
    {
        Line2D line1 = Line2D.CreateLine2D(s1a, s1b);
        Line2D line2 = Line2D.CreateLine2D(s2a, s2b);
        Vector2? lineIntersect = Line2D.IntersectLine(line1, line2);
        if (lineIntersect == null) intersect = null;
        else if (!Line2D.PointOnSegment(lineIntersect.Value, s1a, s1b)) intersect = null;
        else if (!Line2D.PointOnSegment(lineIntersect.Value, s2a, s2b)) intersect = null;
        else intersect = lineIntersect;
        overlap = Line2D.IsOverlap(line1, line2);
    }

    public static void LinesIntersect(Vector2 p1, Vector2 d1, Vector2 p2, Vector2 d2, out Vector2? intersect, out bool overlap)
    {
        Vector2 line1_point2 = p1 + d1;
        Line2D line1 = Line2D.CreateLine2D(p1, line1_point2);        
        Vector2 line2_point2 = p2 + d2;
        Line2D line2 = Line2D.CreateLine2D(p2, line2_point2);
        intersect = Line2D.IntersectLine(line1, line2);
        overlap = Line2D.IsOverlap(line1, line2);
    }
    #endregion

    #region Point check
    public static bool IsPointInTriangle(Vector2 pt, Vector2 v1, Vector2 v2, Vector2 v3)
    {
        var poly = new List<Vector2> { v1, v2, v3 };
        return IsPointInPoly(pt, poly);
    }

    private static bool _RecursiveTestInPolyWithRay(Vector2 p, Vector2 directionalVector, List<Vector2> poly)
    {
        int intersectCount = 0;
        for (int i = 0; i < poly.Count; i += 1)
        {
            Vector2 point_1 = poly[i];
            Vector2 point_2 = i == poly.Count - 1 ? poly[0] : poly[i + 1];
            if (IsPointOnSegment(p, point_1, point_2)) return true;
            
            bool overlap;
            LineSegmentIntersect(p, directionalVector, point_1, point_2, out Vector2? intersect, out overlap);
            if (overlap)
            {
                if (p.x < point_1.x && p.x < point_2.x) intersectCount += 1;
            }
            else if (intersect != null)
            {
                if ((intersect.Value - point_1).magnitude == 0 || (intersect.Value - point_2).magnitude == 0)
                {
                    directionalVector.y -= 0.1f;
                    return _RecursiveTestInPolyWithRay(p, directionalVector, poly);
                }

                if (
                    IsPointOnSegment(intersect.Value, point_1, point_2) &&
                    p.x < intersect.Value.x
                ) intersectCount += 1;
            }
        }
    
        return intersectCount % 2 == 1;
    }
    public static bool IsPointInPoly(Vector2 p, List<Vector2> poly)
    {
        return _RecursiveTestInPolyWithRay(p, new Vector2(1, 100000), poly);
    }

    public static bool IsPointOnSegment(Vector2 p, Vector2 sa, Vector2 sb)
    {
        return Line2D.PointOnSegment(p, sa, sb);
    }
    #endregion

    #region Vector2
    public static Rect GetVectorsRect(IEnumerable<Vector2> vecs)
    {
        float maxX = -999999;
        float minX = 999999;        
        float maxY = -999999;
        float minY = 999999;

        foreach (var item in vecs)
        {
            if (item.x > maxX) maxX = item.x;
            if (item.y > maxY) maxY = item.y;
            if (item.x < minX) minX = item.x;
            if (item.y < minY) minY = item.y;
        }

        return new Rect(minX, minY, (maxX - minX), (maxY - minY));
    }

    /// <summary>
    /// Calculate average of vectors. Vector zero if empty list
    /// </summary>
    public static Vector2 AverageVectors(IEnumerable<Vector2> vs)
    {
        Vector2 totalVec = Vector2.zero;
        int count = 0;
        foreach (var item in vs)
        {
            count += 1;
            totalVec += item;
        }

        if (count > 0) totalVec /= count;
        return totalVec;
    }
    #endregion
}
