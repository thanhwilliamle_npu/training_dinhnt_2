﻿using UnityEngine;
using UnityEditor;

[System.Serializable]
public class Data2DClosestPointOnLine : VisualizeMathTestStrategy
{
    public Vector2 point;
    public Vector2 linePoint;
    public Vector2 lineDirection;

    public override void OnInspectorGUI(SerializedProperty prop)
    {
        base.OnInspectorGUI(prop);
        Vector2 closestPoint = MathTest_2D_Questions.ClosestPointOnLine(point, linePoint, lineDirection);
        EditorGUILayout.Vector2Field("Closest Point: ", closestPoint);
    }

    public override void OnDrawGizmos()
    {
        Vector2 closestPoint = MathTest_2D_Questions.ClosestPointOnLine(point, linePoint, lineDirection);
        DrawRedPoint(point);
        DrawLine(linePoint, lineDirection);
        DrawRedPoint(closestPoint);
        DrawBluePoint(linePoint);
    }

    public override void OnSceneGUI()
    {
        HandleMoveForVector(ref point);
        HandleMoveForLine(ref linePoint, ref lineDirection);
    }
}