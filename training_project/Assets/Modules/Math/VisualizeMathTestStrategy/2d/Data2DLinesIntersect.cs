﻿using UnityEngine;
using UnityEditor;

[System.Serializable]
public class Data2DLinesIntersect : VisualizeMathTestStrategy, CanShowResultIntersect
{
    public Vector2 p1;
    public Vector2 d1;
    public Vector2 p2;
    public Vector2 d2;

    public override void OnInspectorGUI(SerializedProperty prop)
    {
        base.OnInspectorGUI(prop);
        MathTest_2D_Questions.LinesIntersect(p1, d1, p2, d2, out Vector2? intersect, out bool overlap);
        this.ShowResultsIntersect(intersect, overlap);
    }

    public override void OnDrawGizmos()
    {
        DrawLine(p1, d1);
        DrawLine(p2, d2);
        MathTest_2D_Questions.LinesIntersect(p1, d1, p2, d2, out Vector2? intersect, out bool overlap);
        if (!overlap && intersect != null) DrawRedPoint(intersect.Value);
        DrawBluePoint(p1);
        DrawBluePoint(p2);
    }

    public override void OnSceneGUI()
    {
        HandleMoveForLine(ref p1, ref d1);
        HandleMoveForLine(ref p2, ref d2);
    }
}