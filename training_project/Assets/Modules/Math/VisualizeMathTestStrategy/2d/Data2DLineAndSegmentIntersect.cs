﻿using UnityEngine;
using UnityEditor;

[System.Serializable]
public class Data2DLineAndSegmentIntersect : VisualizeMathTestStrategy, CanShowResultIntersect
{
    public Vector2 point;
    public Vector2 lineDirection;
    public Vector2 sa;
    public Vector2 sb;

    public override void OnInspectorGUI(SerializedProperty prop)
    {
        base.OnInspectorGUI(prop);
        MathTest_2D_Questions.LineSegmentIntersect(point, lineDirection, sa, sb, out Vector2? intersect, out bool overlap);
        this.ShowResultsIntersect(intersect, overlap);
    }

    public override void OnDrawGizmos()
    {
        DrawLine(point, lineDirection);
        DrawSegment(sa, sb);
        MathTest_2D_Questions.LineSegmentIntersect(point, lineDirection, sa, sb, out Vector2? intersect, out bool overlap);
        if (!overlap && intersect != null) DrawRedPoint(intersect.Value);
        DrawBluePoint(point);
    }

    public override void OnSceneGUI()
    {
        HandleMoveForLine(ref point, ref lineDirection);
        HandleMoveForVector(ref sa);
        HandleMoveForVector(ref sb);
    }
}