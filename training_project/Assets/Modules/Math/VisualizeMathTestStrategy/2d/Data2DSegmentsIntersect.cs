﻿using UnityEngine;
using UnityEditor;

[System.Serializable]
public class Data2DSegmentsIntersect : VisualizeMathTestStrategy, CanShowResultIntersect
{
    public Vector2 s1a;
    public Vector2 s1b;
    public Vector2 s2a;
    public Vector2 s2b;

    public override void OnInspectorGUI(SerializedProperty prop)
    {
        base.OnInspectorGUI(prop);
        MathTest_2D_Questions.SegmentsIntersect(s1a, s1b, s2a, s2b, out Vector2? intersect, out bool overlap);
        this.ShowResultsIntersect(intersect, overlap);
    }

    public override void OnDrawGizmos()
    {
        DrawSegment(s1a, s1b);
        DrawSegment(s2a, s2b);
        MathTest_2D_Questions.SegmentsIntersect(s1a, s1b, s2a, s2b, out Vector2? intersect, out bool overlap);
        if (!overlap && intersect != null) DrawRedPoint(intersect.Value);
    }

    public override void OnSceneGUI()
    {
        HandleMoveForVector(ref s1a);
        HandleMoveForVector(ref s1b);
        HandleMoveForVector(ref s2a);
        HandleMoveForVector(ref s2b);
    }
}