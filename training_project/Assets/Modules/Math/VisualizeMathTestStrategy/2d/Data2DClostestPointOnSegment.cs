﻿using UnityEngine;
using UnityEditor;

[System.Serializable]
public class Data2DClosestPointOnSegment : VisualizeMathTestStrategy
{
    public Vector2 point;
    public Vector2 sa;
    public Vector2 sb;

    public override void OnInspectorGUI(SerializedProperty prop)
    {
        base.OnInspectorGUI(prop);
        Vector2 closestPoint = MathTest_2D_Questions.ClosestPointOnSegment(point, sa, sb);
        EditorGUILayout.Vector2Field("Closest Point: ", closestPoint);
    }

    public override void OnDrawGizmos()
    {
        Vector2 closestPoint = MathTest_2D_Questions.ClosestPointOnSegment(point, sa, sb);
        
        DrawSegment(sa, sb);
        DrawRedPoint(point);
        DrawYellowPoint(closestPoint);
    }

    public override void OnSceneGUI()
    {
        HandleMoveForVector(ref point);
        HandleMoveForVector(ref sa);
        HandleMoveForVector(ref sb);
    }
}