﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[System.Serializable]
public class Data2DGetVectorsRect : VisualizeMathTestStrategy
{
    public List<Vector2> listVector;

    public override void OnInspectorGUI(SerializedProperty prop)
    {
        base.OnInspectorGUI(prop);
    }
    
    public override void OnDrawGizmos()
    {
        Rect rect = MathTest_2D_Questions.GetVectorsRect(listVector);
        DrawRect(rect);
        foreach (var item in listVector) DrawRedPoint(item);
    }

    public override void OnSceneGUI()
    {
        for (int i = 0; i < listVector.Count; i += 1)
        {
            Vector2 vector = listVector[i];
            HandleMoveForVector(ref vector);
            listVector[i] = vector;
        }
    }
}