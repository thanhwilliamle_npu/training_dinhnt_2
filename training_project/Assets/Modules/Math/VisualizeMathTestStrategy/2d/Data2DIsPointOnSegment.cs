﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[System.Serializable]
public class Data2DIsPointOnSegment : VisualizeMathTestStrategy
{
    public Vector2 point;
    public Vector2 sa;
    public Vector2 sb;

    public override void OnInspectorGUI(SerializedProperty prop)
    {
        base.OnInspectorGUI(prop);
        bool res = MathTest_2D_Questions.IsPointOnSegment(point, sa, sb);
        EditorGUILayout.LabelField(res ? "IN" : "OUT");    
    }

    public override void OnDrawGizmos()
    {
        DrawSegment(sa, sb);
        bool res = MathTest_2D_Questions.IsPointOnSegment(point, sa, sb);
        if (res) DrawRedPoint(point);
        else DrawYellowPoint(point);
    }

    public override void OnSceneGUI()
    {
        HandleMoveForVector(ref point);
        HandleMoveForVector(ref sa);
        HandleMoveForVector(ref sb);
    }
}