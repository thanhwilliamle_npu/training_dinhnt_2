﻿using UnityEngine;
using UnityEditor;

[System.Serializable]
public class Data2DIsPointInTriangle : VisualizeMathTestStrategy
{
    public Vector2 point;
    public Vector2 v1;
    public Vector2 v2;
    public Vector2 v3;

    public override void OnInspectorGUI(SerializedProperty prop)
    {
        base.OnInspectorGUI(prop);
        bool res = MathTest_2D_Questions.IsPointInTriangle(point, v1, v2, v3);
        EditorGUILayout.LabelField(res ? "IN" : "OUT");
    }

    public override void OnDrawGizmos()
    {
        DrawSegment(v1, v2);
        DrawSegment(v2, v3);
        DrawSegment(v3, v1);
        
        bool res = MathTest_2D_Questions.IsPointInTriangle(point, v1, v2, v3);
        if (res) DrawRedPoint(point);
        else DrawYellowPoint(point);
    }

    public override void OnSceneGUI()
    {
        HandleMoveForVector(ref point);
        HandleMoveForVector(ref v1);
        HandleMoveForVector(ref v2);
        HandleMoveForVector(ref v3);
    }
}