﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[System.Serializable]
public class Data2DIsPointInPoly : VisualizeMathTestStrategy
{
    public Vector2 point;
    public List<Vector2> poly;

    public override void OnInspectorGUI(SerializedProperty prop)
    {
        base.OnInspectorGUI(prop);
        bool res = MathTest_2D_Questions.IsPointInPoly(point, poly);
        EditorGUILayout.LabelField(res ? "IN" : "OUT");    
    }

    public override void OnDrawGizmos()
    {
        for (int i = 0; i < poly.Count; i += 1)
        {
            Vector2 point_1 = poly[i];
            Vector2 point_2 = i == poly.Count - 1 ? poly[0] : poly[i + 1];
            DrawSegment(point_1, point_2);
        }
        
        bool res = MathTest_2D_Questions.IsPointInPoly(point, poly);
        if (res) DrawRedPoint(point);
        else DrawYellowPoint(point);
    }

    public override void OnSceneGUI()
    {
        HandleMoveForVector(ref point);
        for (int i = 0; i < poly.Count; i += 1)
        {
            Vector2 vector = poly[i];
            HandleMoveForVector(ref vector);
            poly[i] = vector;
        }
    }
}