﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[System.Serializable]
public class Data2DAverageVectors : VisualizeMathTestStrategy
{
    public List<Vector2> listVector;

    public override void OnInspectorGUI(SerializedProperty prop)
    {
        base.OnInspectorGUI(prop);
        
        IEnumerable<Vector2> vecs = ReadVectors();
        Vector2 aveVector = MathTest_2D_Questions.AverageVectors(vecs);
        EditorGUILayout.Vector2Field("Average vector: ", aveVector);
    }

    IEnumerable<Vector2> ReadVectors()
    {
        for (int i = 0; i < listVector.Count; i += 1)
        {
            yield return listVector[i];
        }
    }
    
    public override void OnDrawGizmos()
    {
        IEnumerable<Vector2> vecs = ReadVectors();
        Vector2 aveVector = MathTest_2D_Questions.AverageVectors(vecs);
        DrawSegment(Vector2.zero,aveVector);
        foreach (var item in vecs) DrawRedPoint(item);
    }

    public override void OnSceneGUI()
    {
        for (int i = 0; i < listVector.Count; i += 1)
        {
            Vector2 vector = listVector[i];
            HandleMoveForVector(ref vector);
            listVector[i] = vector;
        }
    }
}