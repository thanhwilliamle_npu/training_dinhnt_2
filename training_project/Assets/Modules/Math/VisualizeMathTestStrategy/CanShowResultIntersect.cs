﻿using UnityEngine;
using UnityEditor;

public interface CanShowResultIntersect
{
};

public static class Z
{
    public static void ShowResultsIntersect(this CanShowResultIntersect a, Vector2? intersect, bool overlap)
    {
        if (overlap)
            EditorGUILayout.LabelField("Overlap");
        else if (intersect == null)
            EditorGUILayout.LabelField("No Intersection");
        else
            EditorGUILayout.Vector2Field("Intersect: ", intersect.Value);
    }
}