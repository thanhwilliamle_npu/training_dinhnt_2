﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[System.Serializable]
public class Data3DGetBounds : VisualizeMathTestStrategy
{
    public List<Vector3> vectorList;
    public override void OnInspectorGUI(SerializedProperty prop)
    {
        base.OnInspectorGUI(prop);
        Bounds bounds = MathTest_3D_Questions.GetBounds(vectorList);
        EditorGUILayout.BoundsField("Bounds: ", bounds);
    }

    public override void OnDrawGizmos()
    {
        Bounds bounds = MathTest_3D_Questions.GetBounds(vectorList);
        DrawCube(bounds);
    }

    public override void OnSceneGUI()
    {
        for (int i = 0; i < vectorList.Count; i += 1)
        {
            Vector3 vector = vectorList[i];
            HandleMoveForVector(ref vector);
            vectorList[i] = vector;
        }
    }
}