﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[System.Serializable]
public class Data3DAverageVectors : VisualizeMathTestStrategy
{
    public List<Vector3> vectorList;
    public override void OnInspectorGUI(SerializedProperty prop)
    {
        base.OnInspectorGUI(prop);
        Vector3 average = MathTest_3D_Questions.AverageVectors(vectorList);
        EditorGUILayout.Vector3Field("Average: ", average);
    }

    public override void OnDrawGizmos()
    {
        Vector3 average = MathTest_3D_Questions.AverageVectors(vectorList);
        foreach (var item in vectorList)
        {
            DrawSegment(Vector3.zero, item);
        }
        
        Gizmos.color = Color.magenta;
        Gizmos.DrawLine(Vector3.zero, average);
    }

    public override void OnSceneGUI()
    {
        for (int i = 0; i < vectorList.Count; i += 1)
        {
            Vector3 vector = vectorList[i];
            HandleMoveForVector(ref vector);
            vectorList[i] = vector;
        }
    }
}