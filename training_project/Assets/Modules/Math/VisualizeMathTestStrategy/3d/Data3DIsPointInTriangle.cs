﻿using UnityEngine;
using UnityEditor;

[System.Serializable]
public class Data3DIsPointInTriangle : VisualizeMathTestStrategy
{
    public Vector3 point;
    public Vector3 sA;
    public Vector3 sB;
    public Vector3 sC;

    public override void OnInspectorGUI(SerializedProperty prop)
    {
        base.OnInspectorGUI(prop);
        bool isIn = MathTest_3D_Questions.IsPointInTriangle(point, sA, sB, sC);
        EditorGUILayout.LabelField("Relative position: " + (isIn ? "IN" : "OUT"));
    }

    public override void OnDrawGizmos()
    {
        DrawSegment(sA, sB);
        DrawSegment(sB, sC);
        DrawSegment(sC, sA);
        
        bool isIn = MathTest_3D_Questions.IsPointInTriangle(point, sA, sB, sC);
        if (isIn) DrawRedPoint(point);
        else DrawYellowPoint(point);
    }

    public override void OnSceneGUI()
    {
        HandleMoveForVector(ref point);
        HandleMoveForVector(ref sA);
        HandleMoveForVector(ref sB);
        HandleMoveForVector(ref sC);
    }
}