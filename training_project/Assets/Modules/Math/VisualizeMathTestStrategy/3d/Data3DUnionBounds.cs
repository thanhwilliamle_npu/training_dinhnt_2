﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[System.Serializable]
public class Data3DUnionBounds : VisualizeMathTestStrategy
{
    public List<Bounds> boundsList;
    public override void OnInspectorGUI(SerializedProperty prop)
    {
        base.OnInspectorGUI(prop);
        Bounds bounds = MathTest_3D_Questions.UnionBounds(boundsList);
        EditorGUILayout.BoundsField("Bounds: ", bounds);
    }

    public override void OnDrawGizmos()
    {
        Bounds bounds = MathTest_3D_Questions.UnionBounds(boundsList);
        for (int i = 0; i < boundsList.Count; i += 1)
        {
            DrawCube(boundsList[i]);
        }

        Gizmos.color = Color.cyan;
        Gizmos.DrawWireCube(bounds.center, bounds.size);
    }

    public override void OnSceneGUI()
    {
        for (int i = 0; i < boundsList.Count; i += 1)
        {
            Bounds bound = boundsList[i];
            HandleForBound(ref bound);
            boundsList[i] = bound;
        }
    }
}