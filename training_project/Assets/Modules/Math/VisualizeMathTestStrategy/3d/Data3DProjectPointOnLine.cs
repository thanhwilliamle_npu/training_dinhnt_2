﻿using UnityEngine;
using UnityEditor;

[System.Serializable]
public class Data3DProjectPointOnLine : VisualizeMathTestStrategy
{
    public Vector3 point;
    public Vector3 linePoint;
    public Vector3 lineDirection;

    public override void OnInspectorGUI(SerializedProperty prop)
    {
        base.OnInspectorGUI(prop);
        Vector3 projected = MathTest_3D_Questions.ProjectPointOnLine(point, linePoint, lineDirection);
        EditorGUILayout.Vector2Field("Projected Point: ", projected);
    }

    public override void OnDrawGizmos()
    {
        Vector3 projected = MathTest_3D_Questions.ProjectPointOnLine(point, linePoint, lineDirection);
        DrawLine(linePoint, lineDirection);
        DrawRedPoint(point);
        DrawRedPoint(projected);
        DrawBluePoint(linePoint);
    }

    public override void OnSceneGUI()
    {
        HandleMoveForVector(ref point);
        HandleMoveForLine(ref linePoint, ref lineDirection);
    }
}