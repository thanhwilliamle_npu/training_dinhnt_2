﻿using UnityEngine;
using UnityEditor;

[System.Serializable]
public class Data3DProjectPointOnSegment : VisualizeMathTestStrategy
{
    public Vector3 point;
    public Vector3 sa;
    public Vector3 sb;

    public override void OnInspectorGUI(SerializedProperty prop)
    {
        base.OnInspectorGUI(prop);
        MathTest_3D_Questions.ProjectPointOnSegment(point, sa, sb, out Vector3 proj, out float factor);
        EditorGUILayout.Vector2Field("Projection: ", proj);
        EditorGUILayout.FloatField("Factor: ", factor);
    }

    public override void OnDrawGizmos()
    {
        MathTest_3D_Questions.ProjectPointOnSegment(point, sa, sb, out Vector3 proj, out float factor);
        DrawSegment(sa, sb);
        DrawSegment(point, proj);
        DrawRedPoint(proj);
        DrawRedPoint(point);

    }

    public override void OnSceneGUI()
    {
        HandleMoveForVector(ref point);
        HandleMoveForVector(ref sa);
        HandleMoveForVector(ref sb);
    }
}