﻿using UnityEngine;
using UnityEditor;

[System.Serializable]
public class Data3DClosestPointOnSegment : VisualizeMathTestStrategy
{
    public Vector3 point;
    public Vector3 sA;
    public Vector3 sB;

    public override void OnInspectorGUI(SerializedProperty prop)
    {
        base.OnInspectorGUI(prop);
        Vector3 vector = MathTest_3D_Questions.ClosestPointOnSegment(point, sA, sB, out float factor);
        EditorGUILayout.FloatField("Factor: ", factor);
        EditorGUILayout.Vector2Field("Closet Point: ", vector);
    }

    public override void OnDrawGizmos()
    {
        Vector3 vector = MathTest_3D_Questions.ClosestPointOnSegment(point, sA, sB, out float factor);
        DrawSegment(sA, sB);
        DrawSegment(point, vector);
        DrawRedPoint(vector);
    }

    public override void OnSceneGUI()
    {
        HandleMoveForVector(ref point);
        HandleMoveForVector(ref sA);
        HandleMoveForVector(ref sB);
    }
}