﻿using UnityEngine;
using UnityEditor;

[System.Serializable]
public class Data3DDistanceBetweenLine : VisualizeMathTestStrategy
{
    public Vector3 s1a;
    public Vector3 s1b;
    public Vector3 s2a;
    public Vector3 s2b;

    public override void OnInspectorGUI(SerializedProperty prop)
    {
        base.OnInspectorGUI(prop);
        float distance = MathTest_3D_Questions.DistanceBetweenLine(s1a, s1b, s2a, s2b, out Vector3 a, out Vector3 b);
        EditorGUILayout.Vector3Field("Point on s1: ", a);
        EditorGUILayout.Vector3Field("Point on s2: ", b);
        EditorGUILayout.FloatField("Distance: ", distance);
    }

    public override void OnDrawGizmos()
    {
        MathTest_3D_Questions.DistanceBetweenLine(s1a, s1b, s2a, s2b, out Vector3 a, out Vector3 b);
        DrawLine(s1a, s1a - s1b);
        DrawLine(s2a, s2a - s2b);
        DrawRedPoint(a);
        DrawRedPoint(b);
        DrawSegment(a, b);
    }

    public override void OnSceneGUI()
    {
        HandleMoveForVector(ref s1a);
        HandleMoveForVector(ref s1b);
        HandleMoveForVector(ref s2a);
        HandleMoveForVector(ref s2b);
    }
}