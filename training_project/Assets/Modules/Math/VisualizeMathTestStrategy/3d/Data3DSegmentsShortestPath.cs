﻿using UnityEngine;
using UnityEditor;

[System.Serializable]
public class Data3DSegmentsShortestPath : VisualizeMathTestStrategy
{
    public Vector3 s1a;
    public Vector3 s1b;
    public Vector3 s2a;
    public Vector3 s2b;

    public override void OnInspectorGUI(SerializedProperty prop)
    {
        base.OnInspectorGUI(prop);
        float length = MathTest_3D_Questions.SegmentsShortestPath(s1a, s1b, s2a, s2b, out float r1, out float r2);
        EditorGUILayout.FloatField("Path length: ", length);
        EditorGUILayout.Vector2Field("Point on s1: ", s1a + r1 * (s1b - s1a).normalized);
        EditorGUILayout.Vector2Field("Point on s2: ", s2a + r2 * (s2b - s2a).normalized);
    }

    public override void OnDrawGizmos()
    {
        MathTest_3D_Questions.SegmentsShortestPath(s1a, s1b, s2a, s2b, out float r1, out float r2);
        Vector3 p1 = s1a + r1 * (s1b - s1a).normalized;
        Vector3 p2 = s2a + r2 * (s2b - s2a).normalized;
        DrawSegment(s1a, s1b);
        DrawSegment(s2a, s2b);
        DrawSegment(p1, p2);
        DrawRedPoint(p1);
        DrawRedPoint(p2);
    }

    public override void OnSceneGUI()
    {
        HandleMoveForVector(ref s1a);
        HandleMoveForVector(ref s1b);
        HandleMoveForVector(ref s2a);
        HandleMoveForVector(ref s2b);
    }
}