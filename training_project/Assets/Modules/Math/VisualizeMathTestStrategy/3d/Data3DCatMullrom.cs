﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[System.Serializable]
public class Data3DCatMullrom : VisualizeMathTestStrategy
{
    public float t;
    public Vector3 tanPoint1;
    public Vector3 start;
    public Vector3 end;
    public Vector3 tanPoint2;
    public override void OnInspectorGUI(SerializedProperty prop)
    {
        base.OnInspectorGUI(prop);
        Vector3 point = MathTest_3D_Questions.GetCatmullRomPosition(t, tanPoint1, start, end, tanPoint2);
        EditorGUILayout.Vector3Field("Point: ", point);
    }

    public override void OnDrawGizmos()
    {
        Vector3 point = MathTest_3D_Questions.GetCatmullRomPosition(t, tanPoint1, start, end, tanPoint2);
        Vector3 p_1;
        Vector3 p_2;
        var length = Math.Truncate((start - end).magnitude * 100);
        for (float i = 0; i < length - 1; i += 1)
        {
            p_1 = MathTest_3D_Questions.GetCatmullRomPosition(i / (float)length, tanPoint1, start, end, tanPoint2);
            p_2 = MathTest_3D_Questions.GetCatmullRomPosition((i + 1) / (float)length, tanPoint1, start, end, tanPoint2);
            Gizmos.DrawLine(p_1, p_2);
        }
        DrawBluePoint(point);
        DrawSegment(start, tanPoint1);
        DrawSegment(end, tanPoint2);
    }

    public override void OnSceneGUI()
    {
        HandleMoveForVector(ref tanPoint1);
        HandleMoveForVector(ref start);
        HandleMoveForVector(ref end);
        HandleMoveForVector(ref tanPoint2);
    }
}