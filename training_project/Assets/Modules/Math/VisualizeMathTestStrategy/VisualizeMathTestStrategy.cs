﻿using UnityEditor;
using UnityEngine;

public abstract class VisualizeMathTestStrategy
{
    public virtual void OnInspectorGUI(SerializedProperty property)
    {
        EditorGUILayout.PropertyField(property);
    }

    public abstract void OnDrawGizmos();
    public abstract void OnSceneGUI();
    
    protected void DrawRedPoint(Vector3 p)
    {
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(p, 0.05f);
    }
    protected void DrawYellowPoint(Vector3 p)
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawSphere(p, 0.05f);
    }
    protected void DrawBluePoint(Vector3 p)
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawSphere(p, 0.05f);
    }
    protected void DrawLine(Vector3 p, Vector3 dir)
    {
        Gizmos.color = Color.cyan;
        Gizmos.DrawLine(p - dir * 100, p + dir * 100);
    }
    protected void DrawSegment(Vector3 a, Vector3 b)
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawLine(a, b);
    }

    protected void DrawCube(Bounds bounds)
    {
        Gizmos.color = Color.white;
        Gizmos.DrawWireCube(bounds.center, bounds.size);
    }
    
    protected void DrawRect(Rect rect)
    {
        Gizmos.color = Color.white;
        Gizmos.DrawWireCube(rect.center, rect.size);
    }

    protected void HandleMoveForVector(ref Vector3 vector)
    {
        if (Tools.current != Tool.Move) return;
        Vector3 newValue = Handles.PositionHandle(vector, Quaternion.identity);
        vector = newValue;
    }

    protected void HandleMoveForVector(ref Vector2 vector)
    {
        Vector3 vec = new Vector3(vector.x, vector.y, 0);
        HandleMoveForVector(ref vec);
        vector.x = vec.x;
        vector.y = vec.y;
    }

    protected void HandleMoveForLine(ref Vector3 point, ref Vector3 dir)
    {
        HandleMoveForVector(ref point);
        Vector3 point2 = point + dir;
        HandleMoveForVector(ref point2);
        dir = point2 - point;
    }
    
    protected void HandleMoveForLine(ref Vector2 point, ref Vector2 dir)
    {
        HandleMoveForVector(ref point);
        Vector2 point2 = point + dir;
        HandleMoveForVector(ref point2);
        dir = point2 - point;
    }

    protected void HandleForBound(ref Bounds bound)
    {
        Vector3 center = bound.center;
        Vector3 newValue = Handles.PositionHandle(center, Quaternion.identity);
        bound.center = newValue;
        
        Vector3 max = bound.center + bound.extents;
        Vector3 newMax = Handles.PositionHandle(max, Quaternion.identity);
        bound.size = (newMax - bound.center) * 2;
    }
}
