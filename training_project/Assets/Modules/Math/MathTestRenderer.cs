﻿using UnityEditor;
using UnityEngine;

public class MathTestRenderer : MonoBehaviour
{
    public MathFuncEnum func = MathFuncEnum.D_2D_CLOSEST_POINT_ON_LINE;
    
    // math 2d
    public Data2DClosestPointOnLine data2DClosestPointOnLine;
    public Data2DClosestPointOnSegment data2DClosestPointOnSegment;
    public Data2DLineAndSegmentIntersect data2DLineAndSegmentIntersect;
    public Data2DSegmentsIntersect data2DSegmentsIntersect;
    public Data2DLinesIntersect data2DLinesIntersect;
    public Data2DIsPointInTriangle data2DIsPointInTriangle;
    public Data2DIsPointInPoly data2DIsPointInPoly;
    public Data2DIsPointOnSegment data2DIsPointOnSegment;
    public Data2DGetVectorsRect data2DGetVectorsRect;
    public Data2DAverageVectors data2DAverageVectors;
    
    // math 3d
    public Data3DProjectPointOnLine data3DProjectPointOnLine;
    public Data3DProjectPointOnSegment data3DProjectPointOnSegment;
    public Data3DDistanceBetweenLine data3DDistanceBetweenLine;
    public Data3DSegmentsShortestPath data3DSegmentsShortestPath;
    public Data3DClosestPointOnSegment data3DClosestPointOnSegment;
    public Data3DIsPointInTriangle data3DIsPointInTriangle;
    public Data3DGetBounds data3DGetBounds;
    public Data3DUnionBounds data3DUnionBounds;
    public Data3DAverageVectors data3DAverageVectors;
    public Data3DCatMullrom data3DCatMullrom;
    
    private VisualizeMathTestStrategy _GetVisualizeStrategy()
    {
        switch (func)
        {
            case MathFuncEnum.D_2D_CLOSEST_POINT_ON_LINE:
                return data2DClosestPointOnLine;

            case MathFuncEnum.D_2D_CLOSEST_POINT_ON_SEGMENT:
                return data2DClosestPointOnSegment;
            
            case MathFuncEnum.D_2D_LINE_AND_SEGMENT_INTERSECT:
                return data2DLineAndSegmentIntersect;
            
            case MathFuncEnum.D_2D_SEGMENTS_INTERSECT:
                return data2DSegmentsIntersect;
            
            case MathFuncEnum.D_2D_LINES_INTERSECT:
                return data2DLinesIntersect;
            
            case MathFuncEnum.D_2D_IS_POINT_IN_TRIANGLE:
                return data2DIsPointInTriangle;
            
            case MathFuncEnum.D_2D_IS_POINT_IN_POLY:
                return data2DIsPointInPoly;
            
            case MathFuncEnum.D_2D_IS_POINT_ON_SEGMENT:
                return data2DIsPointOnSegment;
            
            case MathFuncEnum.D_2D_GET_VECTORS_RECT:
                return data2DGetVectorsRect;
            
            case MathFuncEnum.D_2D_AVERAGE_VECTORS:
                return data2DAverageVectors;
            
            case MathFuncEnum.D_3D_PROJECT_POINT_ON_LINE:
                return data3DProjectPointOnLine;       
            
            case MathFuncEnum.D_3D_PROJECT_POINT_ON_SEGMENT:
                return data3DProjectPointOnSegment; 
            
            case MathFuncEnum.D_3D_DISTANCE_BETWEEN_LINES:
                return data3DDistanceBetweenLine;
            
            case MathFuncEnum.D_3D_SEGMENT_SHORTEST_PATH:
                return data3DSegmentsShortestPath;
            
            case MathFuncEnum.D_3D_CLOSEST_POINT_ON_SEGMENT:
                return data3DClosestPointOnSegment;
            
            case MathFuncEnum.D_3D_IS_POINT_IN_TRIANGLE:
                return data3DIsPointInTriangle;
            
            case MathFuncEnum.D_3D_GET_BOUNDS:
                return data3DGetBounds;
            
            case MathFuncEnum.D_3D_UNION_BOUNDS:
                return data3DUnionBounds;
            
            case MathFuncEnum.D_3D_AVERAGE_VECTOR:
                return data3DAverageVectors;
            
            case MathFuncEnum.D_3D_CATMULLROM:
                return data3DCatMullrom;
        }

        return null;
    }

    public void DrawInspector(SerializedObject serializedObject)
    {
        var drawStrategy = _GetVisualizeStrategy();
        var propertyName = _GetVisualizeStrategyPropertyName();
        SerializedProperty property = serializedObject.FindProperty(propertyName);
        if (drawStrategy != null && property!=null)
        {
            drawStrategy.OnInspectorGUI(property);
        }
    }

    public void HandleOnSceneGUI()
    {
        var drawStrategy = _GetVisualizeStrategy();
        if (drawStrategy != null) drawStrategy.OnSceneGUI();
    }

    private string _GetVisualizeStrategyPropertyName()
    {
        switch (func)
        {
            case MathFuncEnum.D_2D_CLOSEST_POINT_ON_LINE:
                return nameof(data2DClosestPointOnLine);
            
            case MathFuncEnum.D_2D_CLOSEST_POINT_ON_SEGMENT:
                return nameof(data2DClosestPointOnSegment);
            
            case MathFuncEnum.D_2D_LINE_AND_SEGMENT_INTERSECT:
                return nameof(data2DLineAndSegmentIntersect);
            
            case MathFuncEnum.D_2D_SEGMENTS_INTERSECT:
                return nameof(data2DSegmentsIntersect);
            
            case MathFuncEnum.D_2D_LINES_INTERSECT:
                return nameof(data2DLinesIntersect);
            
            case MathFuncEnum.D_2D_IS_POINT_IN_TRIANGLE:
                return nameof(data2DIsPointInTriangle);
            
            case MathFuncEnum.D_2D_IS_POINT_IN_POLY:
                return nameof(data2DIsPointInPoly);
            
            case MathFuncEnum.D_2D_IS_POINT_ON_SEGMENT:
                return nameof(data2DIsPointOnSegment);
            
            case MathFuncEnum.D_2D_GET_VECTORS_RECT:
                return nameof(data2DGetVectorsRect);            
            
            case MathFuncEnum.D_2D_AVERAGE_VECTORS:
                return nameof(data2DAverageVectors);
            
            case MathFuncEnum.D_3D_PROJECT_POINT_ON_LINE:
                return nameof(data3DProjectPointOnLine);            
            
            case MathFuncEnum.D_3D_PROJECT_POINT_ON_SEGMENT:
                return nameof(data3DProjectPointOnSegment);
            
            case MathFuncEnum.D_3D_DISTANCE_BETWEEN_LINES:
                return nameof(data3DDistanceBetweenLine);
            
            case MathFuncEnum.D_3D_SEGMENT_SHORTEST_PATH:
                return nameof(data3DSegmentsShortestPath);
            
            case MathFuncEnum.D_3D_CLOSEST_POINT_ON_SEGMENT:
                return nameof(data3DClosestPointOnSegment);
            
            case MathFuncEnum.D_3D_GET_BOUNDS:
                return nameof(data3DGetBounds);
            
            case MathFuncEnum.D_3D_UNION_BOUNDS:
                return nameof(data3DUnionBounds);
            
            case MathFuncEnum.D_3D_AVERAGE_VECTOR:
                return nameof(data3DAverageVectors);
            
            case MathFuncEnum.D_3D_CATMULLROM:
                return nameof(data3DCatMullrom);
        }
        
        return null;
    }

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        var gizmoStrategy = _GetVisualizeStrategy();
        if (gizmoStrategy != null) gizmoStrategy.OnDrawGizmos();
    }
#endif
}