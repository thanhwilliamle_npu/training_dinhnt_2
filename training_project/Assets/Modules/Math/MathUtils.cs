﻿using UnityEngine;

public enum NumberOfSolutionForSerie
{
    UNIQUE = 0,
    INFINITE = 1,
    NONE = 2,
};

public struct EquationSerie2
{
    // ax + by = c;
    public float a1, b1, c1;
    public float a2, b2, c2;

    public static EquationSerie2 CreateEquationSerie2(
        float a1, float b1, float c1,
        float a2, float b2, float c2)
    {
        EquationSerie2 s2;
        s2.a1 = a1; s2.b1 = b1; s2.c1 = c1;
        s2.a2 = a2; s2.b2 = b2; s2.c2 = c2;
        return s2;
    }
}

public struct EquationSerie3
{
    // ax + by + cz = d;
    public float a1, b1, c1, d1;
    public float a2, b2, c2, d2;
    public float a3, b3, c3, d3;

    public static EquationSerie3 CreateEquationSerie3(
        float a1, float b1, float c1, float d1,
        float a2, float b2, float c2, float d2,
        float a3, float b3, float c3, float d3)
    {
        EquationSerie3 s3;
        s3.a1 = a1; s3.b1 = b1; s3.c1 = c1; s3.d1 = d1;
        s3.a2 = a2; s3.b2 = b2; s3.c2 = c2; s3.d2 = d2;
        s3.a3 = a3; s3.b3 = b3; s3.c3 = c3; s3.d3 = d3;
        return s3;
    }
}

public class MathUtils
{
    public static bool FloatEqual(float a, float b)
    {
        // return (a >= b - Mathf.Epsilon && a <= b + Mathf.Epsilon);
        return Mathf.Approximately(a, b);
    }

    public static bool FloatGreater(float a, float b)
    {
        return (a - b) > Mathf.Epsilon;
    }
    
    public static NumberOfSolutionForSerie AnalyzeSolutionForSerie2(EquationSerie2 equationSerie2)
    {
        float a1 = equationSerie2.a1;
        float b1 = equationSerie2.b1;
        float c1 = equationSerie2.c1;
        float a2 = equationSerie2.a2;
        float b2 = equationSerie2.b2;
        float c2 = equationSerie2.c2;
        float D = a1 * b2 - a2 * b1;
        float Dx = c1 * b2 - c2 * b1;
        float Dy = a1 * c2 - a2 * c1;

        if (FloatEqual(0, D))
        {
            if ((!FloatEqual(Dx, 0) || !FloatEqual(Dy, 0)))
                return NumberOfSolutionForSerie.NONE;
            return NumberOfSolutionForSerie.INFINITE;
        }
        return NumberOfSolutionForSerie.UNIQUE;
    }

    public static void SolveEquationSerie2(EquationSerie2 eq, out float? x, out float? y)
    {
        NumberOfSolutionForSerie num = AnalyzeSolutionForSerie2(eq);
        if (num == NumberOfSolutionForSerie.NONE || num == NumberOfSolutionForSerie.INFINITE)
        {
            x = null;
            y = null;
            return;
        }

        float D = eq.a1 * eq.b2 - eq.a2 * eq.b1;
        float Dx = eq.c1 * eq.b2 - eq.c2 * eq.b1;
        float Dy = eq.a1 * eq.c2 - eq.a2 * eq.c1;
        x = Dx / D;
        y = Dy / D;
    }

    public static NumberOfSolutionForSerie AnalyzeSolutionForSerie3(EquationSerie3 equationSerie3)
    {
        return NumberOfSolutionForSerie.UNIQUE;
    }
}