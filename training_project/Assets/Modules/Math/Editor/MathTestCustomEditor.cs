﻿using UnityEditor;

[CustomEditor(typeof(MathTestRenderer))]
[CanEditMultipleObjects]
public class MathTestCustomEditor : Editor
{
    public override void OnInspectorGUI()
    {
        serializedObject.UpdateIfRequiredOrScript();
        EditorGUILayout.PropertyField(serializedObject.FindProperty("func"), true);
        
        var mathRenderer = target as MathTestRenderer;
        mathRenderer.DrawInspector(serializedObject);

        serializedObject.ApplyModifiedProperties();
    }

    private void OnSceneGUI()
    {
        var mathRenderer = target as MathTestRenderer;
        mathRenderer.HandleOnSceneGUI();
    }
}