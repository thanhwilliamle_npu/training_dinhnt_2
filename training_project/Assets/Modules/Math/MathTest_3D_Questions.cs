﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;

struct Line3D
{
    public Vector3 M;
    public Vector3 directionalVector;

    public static Line3D CreateLine3D(Vector3 M, Vector3 directionalVector)
    {
        Line3D line;
        line.M = M;
        line.directionalVector = directionalVector;
        return line;
    }

    public static bool PointOnLine(Vector3 point, Line3D line)
    {
        float a1 = line.directionalVector.x;
        float a2 = line.directionalVector.y;
        float a3 = line.directionalVector.z;
        float lMx = line.M.x;
        float lMy = line.M.y;
        float lMz = line.M.z;
        
        if (a1 == 0 && !MathUtils.FloatEqual(point.x, lMx)) return false;
        if (a2 == 0 && !MathUtils.FloatEqual(point.y, lMy)) return false;
        if (a3 == 0 && !MathUtils.FloatEqual(point.z, lMz)) return false;

        float? t_value = null;
        float cur;
        float[] a = {a1, a2, a3};
        for (int i = 0; i < 3; i += 1)
        {
            if (a[i] != 0)
            {
                cur = (point.y - lMy) / a[i];
                if (t_value != null && !MathUtils.FloatEqual(t_value.Value, cur)) return false;
                t_value = cur;
            }
        }
        return true;
    }
    
    public static Vector3 SelectPointOnLineWithT(Line3D line, float t)
    {
        float a1 = line.directionalVector.x;
        float a2 = line.directionalVector.y;
        float a3 = line.directionalVector.z;
        float lMx = line.M.x;
        float lMy = line.M.y;
        float lMz = line.M.z;
        return new Vector3(
            lMx + a1 * t,
            lMy + a2 * t,
            lMz + a3 * t
        );
    }

    public static bool PointOnSegment(Vector3 p, Vector3 a, Vector3 b)
    {
        return MathUtils.FloatEqual((a - b).magnitude, (a - p).magnitude + (b - p).magnitude);
    }
}

struct Surface3D
{
    public Vector3 M;
    public Vector3 perpenVector;
    public static Surface3D CreateSurface3D(Vector3 M, Vector3 perpenVector)
    {
        Surface3D surface;
        surface.M = M;
        surface.perpenVector = perpenVector;
        return surface;
    }

    public static bool PointOnSurface(Vector3 point, Surface3D surface)
    {
        float sA = surface.perpenVector.x;
        float sB = surface.perpenVector.y;
        float sC = surface.perpenVector.z;
        float sMx = surface.M.x;
        float sMy = surface.M.y;
        float sMz = surface.M.z;

        float lhs = sA * (point.x - sMx) + sB * (point.y - sMy) + sC * (point.z - sMz);
        return MathUtils.FloatEqual(lhs, 0);
    }
}

public static class MathTest_3D_Questions
{
    private static bool _IsVectorParallel(Vector3 v1, Vector3 v2)
    {
        float dotProduct = v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
        float magnitudeProduct = v1.magnitude * v2.magnitude;
        return MathUtils.FloatEqual(Mathf.Abs(dotProduct),Mathf.Abs(magnitudeProduct));
    }
    private static void _IntersectPoint(Line3D line, Surface3D surface3D, out Vector3? result)
    {
        float dotProduct = Vector3.Dot(line.directionalVector, surface3D.perpenVector);
        if (MathUtils.FloatEqual(dotProduct, 0)) result = null; // parallel or overlap, return null
        else result = _IntersectPoint(line, surface3D);
    }
    private static Vector3 _IntersectPoint(Line3D line, Surface3D surface)
    {
        float sA = surface.perpenVector.x;
        float sB = surface.perpenVector.y;
        float sC = surface.perpenVector.z;
        float sMx = surface.M.x;
        float sMy = surface.M.y;
        float sMz = surface.M.z;
        float sD = -sA * sMx - sB * sMy - sC * sMz;

        float a1 = line.directionalVector.x;
        float a2 = line.directionalVector.y;
        float a3 = line.directionalVector.z;
        float lMx = line.M.x;
        float lMy = line.M.y;
        float lMz = line.M.z;

        float t = -(sA * lMx + sB * lMy + sC * lMz + sD) / (sA * a1 + sB * a2 + sC * a3);
        float returnX = lMx + a1 * t;
        float returnY = lMy + a2 * t;
        float returnZ = lMz + a3 * t;
        return new Vector3(returnX, returnY, returnZ);
    }

    private static Vector3? _IntersectPoint(Line3D line1, Line3D line2)
    {
        float a1 = line1.directionalVector.x;
        float b1 = - line2.directionalVector.x;
        float c1 = line2.M.x - line1.M.x;
        
        float a2 = line1.directionalVector.y;
        float b2 = - line2.directionalVector.y;
        float c2 = line2.M.y - line1.M.y;
        
        float a3 = line1.directionalVector.z;
        float b3 = - line2.directionalVector.z;
        float c3 = line2.M.z - line1.M.z;

        EquationSerie2 eq1 = EquationSerie2.CreateEquationSerie2(
            a1, b1, c1,
            a2, b2, c2
        );
        EquationSerie2 eq2 = EquationSerie2.CreateEquationSerie2(
            a1, b1, c1,
            a3, b3, c3
        );
        NumberOfSolutionForSerie num1 = MathUtils.AnalyzeSolutionForSerie2(eq1);
        // NumberOfSolutionForSerie num2 = MathUtils.AnalyzeSolutionForSerie2(eq1);

        float? t1 = null;
        float? t2 = null;
        
        if (num1 == NumberOfSolutionForSerie.INFINITE)
        {
            MathUtils.SolveEquationSerie2(eq2, out t1, out t2);
            if (t1 == null || t2 == null) return null;
            if (MathUtils.FloatEqual(a2 * t1.Value + b2 * t2.Value, c2))
                return Line3D.SelectPointOnLineWithT(line1, t1.Value);
        }
        else
        {
            MathUtils.SolveEquationSerie2(eq1, out t1, out t2);
            if (t1 == null || t2 == null) return null;
            if (MathUtils.FloatEqual(a3 * t1.Value + b3 * t2.Value, c3))
                return Line3D.SelectPointOnLineWithT(line1, t1.Value);
        }
 
        return null;
    }
    
    public static Vector3 ProjectPointOnLine(Vector3 point, Vector3 linePoint, Vector3 lineDir)
    {
        Surface3D surface = Surface3D.CreateSurface3D(point, lineDir);
        Line3D line = Line3D.CreateLine3D(linePoint, lineDir);
        return _IntersectPoint(line, surface);
    }

    public static float DistanceBetweenLine(Vector3 s1a, Vector3 s1b, Vector3 s2a, Vector3 s2b, out Vector3 p1, out Vector3 p2)
    {
        // surface sur1 contain s1 and parallel with s2
        Vector3 perpendicularVectorSur1 = Vector3.Cross((s1b - s1a), (s2b - s2a));
        if (perpendicularVectorSur1 == Vector3.zero)
        {
            // 2 segment having the same direction, choose arbitrary perpendicular vector
            perpendicularVectorSur1.x = - 1 / (s1b - s1a).x;
            perpendicularVectorSur1.y = 1 / (s1b - s1a).y;
            perpendicularVectorSur1.z = 1 / (s1b - s1a).z;
        }
        Surface3D sur1 = Surface3D.CreateSurface3D(s1a, perpendicularVectorSur1);
        
        // surface surPen contain s1 and perpendicular with sur1
        Vector3 perpendicularVectorSur2 = Vector3.Cross((s1b - s1a), perpendicularVectorSur1);
        Surface3D sur2 = Surface3D.CreateSurface3D(s1a, perpendicularVectorSur2);
        
        // point M = sur2 intersect s2
        Line3D s2 = Line3D.CreateLine3D(s2a, s2b - s2a);
        Vector3 point_M = _IntersectPoint(s2, sur2);

        // d perpen with both s1 and s2
        Line3D d = Line3D.CreateLine3D(point_M, perpendicularVectorSur1);
        
        // point N = d intersect sur1
        Vector3 point_N = _IntersectPoint(d, sur1);

        p1 = point_N;
        p2 = point_M;
        return (point_N - point_M).magnitude;
    }
    public static float SegmentsShortestPath(Vector3 s1a, Vector3 s1b, Vector3 s2a, Vector3 s2b, 
        out float r1, out float r2)
    {
        // surface sur1 contain s1 and parallel with s2
        Vector3 perpendicularVectorSur1 = Vector3.Cross((s1b - s1a), (s2b - s2a));
        Debug.Log("perpendicularVectorSur1: " + perpendicularVectorSur1);
        
        if (perpendicularVectorSur1 == Vector3.zero)
        {
            // 2 segment having the same direction, choose arbitrary perpendicular vector
            perpendicularVectorSur1.x = - 1 / (s1b - s1a).x;
            perpendicularVectorSur1.y = 1 / (s1b - s1a).y;
            perpendicularVectorSur1.z = 1 / (s1b - s1a).z;
        }
        Surface3D sur1 = Surface3D.CreateSurface3D(s1a, perpendicularVectorSur1);
        
        // proj of s2a on sur1
        Line3D line1 = Line3D.CreateLine3D(s2a, perpendicularVectorSur1);
        Vector3 p2a = _IntersectPoint(line1, sur1);

        // proj of s2b on sur1
        Line3D line2 = Line3D.CreateLine3D(s2b, perpendicularVectorSur1);
        Vector3 p2b = _IntersectPoint(line2, sur1);

        Vector3 res_1 = Vector3.zero; // point on s1
        Vector3 proj_2 = Vector3.zero; // point on proj
        float mag = (p2a - s1a).magnitude;
        
        Line3D s1 = Line3D.CreateLine3D(s1a, (s1b - s1a));
        Line3D p2 = Line3D.CreateLine3D(p2a, (p2b - p2a));

        // intersection of p2 and s1
        Vector3? intersect = _IntersectPoint(s1, p2);
        if (intersect != null && 
            Line3D.PointOnSegment(intersect.Value, s1a, s1b) &&
            Line3D.PointOnSegment(intersect.Value, p2b, p2a)
            )
        {
            res_1 = intersect.Value;
            proj_2 = intersect.Value;
        }
        else
        {
            void CompareResultAndAssign(Vector3 point, Vector3 point_1, Vector3 point_2, ref Vector3 pointOut, ref Vector3 pointOnLine)
            {
                Vector3 test = ClosestPointOnSegment(point, point_1, point_2, out float factor);
                float newMag = (test - point).magnitude;
                if (newMag < mag)
                {
                    pointOnLine = test;
                    pointOut = point;
                    mag = newMag;
                }
            }
        
            CompareResultAndAssign(p2a, s1a, s1b, ref proj_2, ref res_1);
            CompareResultAndAssign(p2b, s1a, s1b, ref proj_2, ref res_1);
            CompareResultAndAssign(s1a, p2a, p2b, ref res_1, ref proj_2);
            CompareResultAndAssign(s1b, p2a, p2b, ref res_1, ref proj_2);
        }

        Line3D line3 = Line3D.CreateLine3D(proj_2, perpendicularVectorSur1);
        Line3D s2 = Line3D.CreateLine3D(s2a, (s2b - s2a));
        Vector3 res_2 = _IntersectPoint(s2, line3).Value;

        r1 = (res_1 - s1a).magnitude;
        r2 = (res_2 - s1a).magnitude;
        return mag;
    }

    /// <summary>
    /// Project <paramref name="point"/> onto line segment from <paramref name="sA"/> to <paramref name="sB"/>. <para/>
    /// Outcome is the projected point and lerp factor from <paramref name="sA"/> to <paramref name="sB"/>. <para/>
    /// Return true if the projected point is within the segment
    /// </summary>
    /// <param name="point">Starting point</param>
    /// <param name="sA">Segment point A</param>
    /// <param name="sB">Segment point B</param>
    /// <param name="proj">Projected point</param>
    /// <param name="factor">Projected point lerp factor from A->B</param>
    /// <returns>true if the projected point is within the segment</returns>
    public static bool ProjectPointOnSegment(Vector3 point, Vector3 sA, Vector3 sB, out Vector3 proj, out float factor)
    {
        Vector3 perpenVec = sB - sA;
        Surface3D sur = Surface3D.CreateSurface3D(point, perpenVec);
        Line3D AB = Line3D.CreateLine3D(sA, sB - sA);
        proj = _IntersectPoint(AB, sur);
        factor = ((proj - sA).magnitude) / ((sB - sA).magnitude);
        if (Vector3.Dot(sB - sA, proj - sA) < 0) factor *= -1;
        return factor >= 0 && factor <= 1;
    }

    public static Vector3 ClosestPointOnSegment(Vector3 point, Vector3 sA, Vector3 sB, out float factor)
    {
        Vector3 proj;
        ProjectPointOnSegment(point, sA, sB, out proj, out factor);
        if (factor < 0)
        {
            factor = 0;
            return sA;
        }
        
        if (factor > 1)
        {
            factor = 1;
            return sB;
        }

        return proj;
    }

    public static bool IsPointInTriangle(Vector3 p, Vector3 t1, Vector3 t2, Vector3 t3)
    {
        // throw new NotImplementedException();
        float a1 = Vector3.Angle(t1 - p, t2 - p);
        float a2 = Vector3.Angle(t2 - p, t3 - p);
        float a3 = Vector3.Angle(t3 - p, t1 - p);
        return MathUtils.FloatEqual(a1 + a2 + a3, 360);
    }

    // copied from github JPBotelho, tyvm <3 
    public static Vector3 GetCatmullRomPosition(float t, Vector3 tanPoint1, Vector3 start, Vector3 end, Vector3 tanPoint2)
    {
        // Hermite curve formula:
        // (2t^3 - 3t^2 + 1) * p0 + (t^3 - 2t^2 + t) * m0 + (-2t^3 + 3t^2) * p1 + (t^3 - t^2) * m1
        // Vector3 position = (2.0f * t * t * t - 3.0f * t * t + 1.0f) * start
        //                    + (t * t * t - 2.0f * t * t + t) * tanPoint1
        //                    + (-2.0f * t * t * t + 3.0f * t * t) * end
        //                    + (t * t * t - t * t) * tanPoint2;

        Vector3 P0 = tanPoint1;
        Vector3 P1 = start;
        Vector3 P2 = end;
        Vector3 P3 = tanPoint2;
        Vector3 position = 0.5f * ((2 * P1) +
                                  (-P0 + P2) * t +
                                  (2 * P0 - 5 * P1 + 4 * P2 - P3) * t * t +
                                  (-P0 + 3 * P1 - 3 * P2 + P3) * t * t * t);
        return position;
    }

    public static Bounds GetBounds(IEnumerable<Vector3> vs)
    {
        float maxX = Single.NegativeInfinity;
        float minX = Single.PositiveInfinity;     
        float maxY = Single.NegativeInfinity;
        float minY = Single.PositiveInfinity;     
        float maxZ = Single.NegativeInfinity;
        float minZ = Single.PositiveInfinity;  

        // if (vs.Count() == 0) 

        int count = 0;
        foreach (var item in vs)
        {
            if (item.x > maxX) maxX = item.x;
            if (item.y > maxY) maxY = item.y;
            if (item.z > maxZ) maxZ = item.z;

            if (item.x < minX) minX = item.x;
            if (item.y < minY) minY = item.y;
            if (item.z < minZ) minZ = item.z;

            count += 1;
        }
        
        if (count == 0) return new Bounds(Vector3.zero, Vector3.zero);
        
        Vector3 center = (new Vector3(maxX + minX, maxY + minY, maxZ + minZ)) / 2;
        Vector3 size = new Vector3(maxX - minX, maxY - minY, maxZ - minZ);
        return new Bounds(center, size);
    }

    private static Vector3 _AbsVector(Vector3 vec)
    {
        Vector3 a = new Vector3();
        a.x = Mathf.Abs(vec.x);
        a.y = Mathf.Abs(vec.y);
        a.z = Mathf.Abs(vec.z);
        return a;
    }
    
    public static Bounds UnionBounds(IEnumerable<Bounds> bounds)
    {
        float maxX = Single.NegativeInfinity;
        float minX = Single.PositiveInfinity;     
        float maxY = Single.NegativeInfinity;
        float minY = Single.PositiveInfinity;     
        float maxZ = Single.NegativeInfinity;
        float minZ = Single.PositiveInfinity;  

        int count = 0;
        foreach (var item in bounds)
        {
            var maxOfItem = item.center + _AbsVector(item.size) / 2;
            if (maxOfItem.x > maxX) maxX = maxOfItem.x;
            if (maxOfItem.y > maxY) maxY = maxOfItem.y;
            if (maxOfItem.z > maxZ) maxZ = maxOfItem.z;

            var minOfItem = item.center - _AbsVector(item.size) / 2;
            if (minOfItem.x < minX) minX = minOfItem.x;
            if (minOfItem.y < minY) minY = minOfItem.y;
            if (minOfItem.z < minZ) minZ = minOfItem.z;

            count += 1;
        }

        if (count == 0) return new Bounds(Vector3.zero, Vector3.zero);
        
        Vector3 center = (new Vector3(maxX + minX, maxY + minY, maxZ + minZ)) / 2;
        Vector3 size = new Vector3(maxX - minX, maxY - minY, maxZ - minZ);
        return new Bounds(center, size);
    }

    public static Vector3 AverageVectors(IEnumerable<Vector3> vs)
    {
        Vector3 totalVec = Vector3.zero;
        foreach (var item in vs) totalVec += item;

        int count = vs.Count();
        if (count > 0) totalVec /= count;
        return totalVec;
    }

}
