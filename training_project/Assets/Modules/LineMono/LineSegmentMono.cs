﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[System.Serializable]
public class LineSegment
{
    public Vector3 A;
    public Vector3 B;
}

public class LineSegmentMono : MonoBehaviour
{
    [HideInInspector] public LineSegment segment;

    public static Color Selected
    {
        get { return GetColorFromPrefs("LineSegmentMono_selectedColor"); }
        set { SaveColorToPrefs(value, "LineSegmentMono_selectedColor"); }
    }

    public static Color NotSelected
    {
        get { return GetColorFromPrefs("LineSegmentMono_notSelectedColor"); }
        set { SaveColorToPrefs(value, "LineSegmentMono_notSelectedColor"); }
    }

    public static void SaveColorToPrefs(Color color, String colorKey)
    {
        EditorPrefs.SetFloat(colorKey + "r", color.r);
        EditorPrefs.SetFloat(colorKey + "g", color.g);
        EditorPrefs.SetFloat(colorKey + "b", color.b);
        EditorPrefs.SetFloat(colorKey + "a", color.a);
    }

    public static Color GetColorFromPrefs(String colorKey)
    {
        float r = EditorPrefs.GetFloat(colorKey + "r", 0);
        float g = EditorPrefs.GetFloat(colorKey + "g", 0);
        float b = EditorPrefs.GetFloat(colorKey + "b", 0);
        float a = EditorPrefs.GetFloat(colorKey + "a", 1);
        return new Color(r, g, b, a);
    }
    

    public float GetSegmentLength()
    {
        return (segment.A - segment.B).magnitude;
    }

    public Vector3 GetCenter()
    {
        return (segment.A + segment.B) / 2;
    }

    private float GetAngleBetweenVector(Vector3 a, Vector3 b)
    {
        if (a.magnitude == 0 || b.magnitude == 0) return 0;
        float cosValue = Mathf.Abs(a.x * b.x + a.y * b.y + a.z * b.z) / a.magnitude * b.magnitude;
        return Mathf.Acos(cosValue) * 180 / Mathf.PI;
    }

    public Vector3 GetAnglesWrbMainCoordinate()
    {
        Vector3 directionalVector = segment.A - segment.B;
        float x = GetAngleBetweenVector(directionalVector, new Vector3(1, 0, 0));
        float y = GetAngleBetweenVector(directionalVector, new Vector3(0, 1, 0));
        float z = GetAngleBetweenVector(directionalVector, new Vector3(0, 0, 1));
        return new Vector3(x, y, z);
    }

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        return;
        if (!UnityEditor.Selection.Contains(gameObject)) Gizmos.color = NotSelected;
        else Gizmos.color = Selected;
        Debug.Log(Gizmos.color);
        Gizmos.DrawLine(segment.A, segment.B);
    }
#endif
}