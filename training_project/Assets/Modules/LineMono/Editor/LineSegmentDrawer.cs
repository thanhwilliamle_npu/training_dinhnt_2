﻿// using System;
// using System.Collections;
// using System.Collections.Generic;

using System;
using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(LineSegment))]
public class LineSegmentDrawer : PropertyDrawer
{
    private Rect createRectAtIndex(Rect position, int idx)
    {
        float singleLineHeight = EditorGUIUtility.singleLineHeight;
        float padding = 4;
        float startY = position.y;
        return new Rect(position.x, startY + (padding + singleLineHeight) * idx, position.width,
            EditorGUIUtility.singleLineHeight);
    }

    private Rect createRectAtIndex(Rect position, int verticalIdx, float percentageX, float percentageWidth)
    {
        float singleLineHeight = EditorGUIUtility.singleLineHeight;
        float padding = 4;
        float startY = position.y;
        return new Rect(
            position.x + position.width * percentageX / 100,
            startY + (padding + singleLineHeight) * verticalIdx,
            position.width * percentageWidth / 100,
            EditorGUIUtility.singleLineHeight
        );
    }

    // Draw the property inside the given rect
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        // Using BeginProperty / EndProperty on the parent property means that
        // prefab override logic works on the entire property.
        EditorGUI.BeginProperty(position, label, property);
        EditorGUI.indentLevel++;

        var oldWideMode = EditorGUIUtility.wideMode;
        EditorGUIUtility.wideMode = true;
        
        // foldout
        var rect_foldout = createRectAtIndex(position, 0);
        // if (property.isExpanded) 
        //     rect_foldout.y = position.y + 9 - position.height / 2; // magic number
        
        property.isExpanded = EditorGUI.Foldout (rect_foldout, property.isExpanded, label, true);
        if (!property.isExpanded)
        {
            EditorGUI.EndProperty();
            return;
        }

        
        var propA = property.FindPropertyRelative("A");
        var propB = property.FindPropertyRelative("B");
        var posA = propA.vector3Value;
        var posB = propB.vector3Value;
        
        // update length and position
        var rect_positionA = createRectAtIndex(position, 1);
        var rect_positionB = createRectAtIndex(position, 2);
        var rect_length = createRectAtIndex(position, 3);

        EditorGUI.PropertyField(rect_positionA, propA, new GUIContent("Position A"));
        EditorGUI.PropertyField(rect_positionB, propB, new GUIContent("Position B"));

        float length = (posA - posB).magnitude;
        var center = (posA + posB) / 2;
        var directionalVector = posA - posB;
        var dvLength = directionalVector.magnitude;
        
        float newLength = EditorGUI.FloatField(rect_length, "Length", length);
        if (newLength != length)
        {
            var ratio = dvLength != 0 ? (newLength / dvLength) : 0;
            posA = center + ratio * directionalVector / 2;
            posB = center - ratio * directionalVector / 2;
            propA.vector3Value = posA;
            propB.vector3Value = posB;
        }

        // update rotation
        EditorGUI.PrefixLabel(createRectAtIndex(position, 4), GUIUtility.GetControlID(FocusType.Passive), new GUIContent("Rotate"));
        var rect_button_1 = createRectAtIndex(position, 4, 38.3f, 20);
        var rect_button_2 = createRectAtIndex(position, 4, 59, 20);
        var rect_button_3 = createRectAtIndex(position, 4, 79.7f, 20);

        if (GUI.Button(rect_button_1, "Ox"))
        {
            posA.y = center.y;
            posA.z = center.z;
            posB.y = center.y;
            posB.z = center.z;
            posA.x = center.x + dvLength / 2;
            posB.x = center.x - dvLength / 2;
            
            propA.vector3Value = posA;
            propB.vector3Value = posB;
        }
        if (GUI.Button(rect_button_2, "Oy"))
        {
            posA.x = center.x;
            posA.z = center.z;
            posB.x = center.x;
            posB.z = center.z;
            posA.y = center.y + dvLength / 2;
            posB.y = center.y - dvLength / 2;
            
            propA.vector3Value = posA;
            propB.vector3Value = posB;
        }

        if (GUI.Button(rect_button_3, "Oz"))
        {
            posA.y = center.y;
            posA.x = center.x;
            posB.y = center.y;
            posB.x = center.x;
            posA.z = center.z + dvLength / 2;
            posB.z = center.z - dvLength / 2;
            
            propA.vector3Value = posA;
            propB.vector3Value = posB;
        }
        
        // button  project
        EditorGUI.PrefixLabel(createRectAtIndex(position, 5), GUIUtility.GetControlID(FocusType.Passive), new GUIContent("Project"));
        var rect_button_rotate_1 = createRectAtIndex(position, 5, 38.3f, 20);
        var rect_button_rotate_2 = createRectAtIndex(position, 5, 59, 20);
        var rect_button_rotate_3 = createRectAtIndex(position, 5, 79.7f, 20);
        if (GUI.Button(rect_button_rotate_1, "Ox"))
        {
            posA.z = 0;
            posB.z = 0;
            posA.y = 0;
            posB.y = 0;
            
            propA.vector3Value = posA;
            propB.vector3Value = posB;
        }
        if (GUI.Button(rect_button_rotate_2, "Oy"))
        {
            posA.x = 0;
            posB.x = 0;
            posA.z = 0;
            posB.z = 0;
            
            propA.vector3Value = posA;
            propB.vector3Value = posB;
        }
        if (GUI.Button(rect_button_rotate_3, "Oz"))
        {
            posA.x = 0;
            posB.x = 0;
            posA.y = 0;
            posB.y = 0;
            
            propA.vector3Value = posA;
            propB.vector3Value = posB;
        }

        EditorGUI.indentLevel--;
        EditorGUIUtility.wideMode = oldWideMode;
        EditorGUI.EndProperty();
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return property.isExpanded ? EditorGUIUtility.singleLineHeight * 6 + 4 * 5 : EditorGUIUtility.singleLineHeight;
    }
}