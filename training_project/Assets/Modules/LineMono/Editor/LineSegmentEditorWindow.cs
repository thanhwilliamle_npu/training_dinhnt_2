﻿using System;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

public class LineSegmentEditorWindow : EditorWindow
{
    public LineSegmentMono obj = null;

    // Add menu named "LineSegmentEditorWindow" to the Window menu
    [MenuItem("Window/LineSegmentEditorWindow")]
    static void Init()
    {
        // Get existing open window or if none, make a new one:
        LineSegmentEditorWindow window =
            (LineSegmentEditorWindow) EditorWindow.GetWindow(typeof(LineSegmentEditorWindow));
        window.Show();
        
    }

    private void OnEnable()
    {
        autoRepaintOnSceneChange = true;
    }

    void OnGUI()
    {
        obj = (LineSegmentMono) EditorGUILayout.ObjectField("LineSegmentMono", obj, typeof(LineSegmentMono), true);
        if (!obj)
        {
            EditorGUILayout.LabelField(new GUIContent("Please select a LineSegmentMono From the Scene"));
        }
        else
        {
            var a = Editor.CreateEditor(obj);
            a.OnInspectorGUI();
            Object.DestroyImmediate(a);
        }
        
        Repaint();
    }

}