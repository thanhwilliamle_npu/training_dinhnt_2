﻿using System;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(LineSegmentMono))]
[CanEditMultipleObjects]
public class LineSegmentMonoEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        var line = target as LineSegmentMono;

        EditorGUILayout.BeginVertical(EditorStyles.helpBox);
        serializedObject.UpdateIfRequiredOrScript();
        EditorGUILayout.PropertyField(serializedObject.FindProperty("segment"));
        serializedObject.ApplyModifiedProperties();
        EditorGUILayout.EndVertical();
        
        EditorGUI.BeginDisabledGroup(true);
        EditorGUILayout.FloatField("Length", line.GetSegmentLength());
        EditorGUILayout.Vector3Field("Angle", line.GetAnglesWrbMainCoordinate());
        EditorGUI.EndDisabledGroup();

        LineSegmentMono.Selected = EditorGUILayout.ColorField("Selected", LineSegmentMono.Selected);
        LineSegmentMono.NotSelected = EditorGUILayout.ColorField("Not elected", LineSegmentMono.NotSelected);

        EditorGUI.BeginDisabledGroup(true);
        EditorGUILayout.BoundsField("Bounds", new Bounds(line.segment.A, line.segment.B));
        EditorGUI.EndDisabledGroup();
    }

    // private void OnSceneGUI()
    // {
    //     var line = target as LineSegmentMono;
    //     // Handles.matrix = line.transform.localToWorldMatrix;
    //     Handles.matrix = Matrix4x4.TRS(Vector3.right * 20, Quaternion.FromToRotation(Vector3.forward, Vector3.up), Vector3.one);
    //     Handles.DrawLine(Vector3.zero, Vector3.one);
    //     Handles.matrix = Matrix4x4.identity;
    // }

    public void OnSceneGUI()
    {
        var line = target as LineSegmentMono;
        Color color = LineSegmentMono.Selected;

        Quaternion handleRot = Tools.pivotRotation == PivotRotation.Global ? Quaternion.identity : line.transform.rotation; 
            
        var posA = line.transform.TransformPoint(line.segment.A);
        var posB = line.transform.TransformPoint(line.segment.B);
        Vector3 bToA = posA - posB;
        Vector3 center = (posA + posB) / 2;

        // var posA = line.segment.A;
        // var posB = line.segment.B;
        Handles.color = color;
        Handles.DrawLine(posA, posB);

        color.a = 0.25f;
        Handles.color = color;
        Handles.DrawWireCube(
            center,
            bToA
        );

        EditorGUI.BeginChangeCheck();
        Vector3? newTargetPositionA = null;
        Vector3? newTargetPositionB = null;
        Vector3? newTargetPositionCenter = null;
        if (Tools.current == UnityEditor.Tool.Move)
        {
            newTargetPositionA = Handles.PositionHandle(posA, handleRot);
            newTargetPositionB = Handles.PositionHandle(posB, handleRot);
            newTargetPositionCenter = Handles.PositionHandle(center, handleRot);
        }

        Quaternion? newQuaternion = null;
        var baseVector = Vector3.forward;
        if (Tools.current == UnityEditor.Tool.Rotate)
        {
            Quaternion rot = Quaternion.FromToRotation(baseVector, bToA);
            newQuaternion = Handles.RotationHandle(rot, center);
        }

        Vector3? newScale = null;
        if (Tools.current == UnityEditor.Tool.Scale)
        {
            newScale = Handles.ScaleHandle(bToA, center, handleRot, 1);
        }
        
        if (EditorGUI.EndChangeCheck())
        {
            if (newTargetPositionA != null) posA = newTargetPositionA.Value;
            if (newTargetPositionB != null) posB = newTargetPositionB.Value;

            if (newTargetPositionCenter != null)
            {
                Vector3 gapVector = newTargetPositionCenter.Value - center;
                posA += gapVector;
                posB += gapVector;
            }
            
            if (newQuaternion != null)
            {
                // var rotDelta = Quaternion.Inverse(rot) * newRot;
                var dir = (newQuaternion.Value * baseVector).normalized;
                var mag = bToA.magnitude;
                posA = center + mag / 2 * dir;
                posB = center - mag / 2 * dir;
            }

            if (newScale != null)
            {
                // var ratio = length != 0 ? (newScale.Value.y / bToA.y) : 0;
                // line.segment.A += center + ratio * bToA / 2;
                // line.segment.B = center - ratio * bToA / 2;
                posA += (newScale.Value - bToA) * 0.5f;
                posB -= (newScale.Value - bToA) * 0.5f;
            }
            
            Undo.RegisterCompleteObjectUndo(target, "Changed Area Of Effect");
            line.segment.A = line.transform.InverseTransformPoint(posA);
            line.segment.B = line.transform.InverseTransformPoint(posB);
        }
    }
}