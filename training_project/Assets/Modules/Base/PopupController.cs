﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopupController : MonoBehaviour
{
    private static PopupController _instance;
    public static PopupController Instance => _instance;
    private void Awake()
    {
        _instance = this;
    }

    [SerializeField] private GuiUserInfo guiUserInfoPrefab;
    public GuiUserInfo ShowGuiUserInfo(VisualizePlayer vP)
    {
        var gui = Instantiate(guiUserInfoPrefab, transform);
        gui.ShowUserInfo(vP);
        return gui;
    }    
    
    [SerializeField] private GuiTeamInfo guiTeamInfoPrefab;
    public GuiTeamInfo ShowTeamInfo(VisibleTeam vTeam)
    {
        var gui = Instantiate(guiTeamInfoPrefab, transform);
        gui.ShowTeamInfo(vTeam);
        return gui;
    }

}
