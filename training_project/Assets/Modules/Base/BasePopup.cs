﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class BasePopup : MonoBehaviour
{
    protected void FxInDoFadeRawImage(GameObject obj)
    {
        obj.GetComponent<RawImage>().DOFade(0, 0.3f).From();
    }

    protected void FxInDoMove(GameObject obj)
    {
        obj.transform.DOMoveY(-300, 0.3f).From().SetEase(Ease.OutQuart);
    }
}
