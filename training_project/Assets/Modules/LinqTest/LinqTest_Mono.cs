﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;
using Debug = UnityEngine.Debug;

public class LinqTest
{
    public List<Player> players = new List<Player>();
    private int _playerId = 0;

    public List<Equipment> equipments = new List<Equipment>();
    private int _equipmentId = 0;

    public List<Team> teams = new List<Team>();
    private int _teamId = 0;

    private Player _CreatePlayer(string name, float hp, float maxHP, float cash, List<string> equipments, string team)
    {
        Player p = new Player();
        p.ID = _playerId.ToString();
        _playerId += 1;
        p.Name = name;
        p.HP = hp;
        p.MaxHP = maxHP;
        p.Cash = cash;
        p.Equipments.AddRange(equipments);
        p.Team = team;
        players.Add(p);
        return p;
    }

    private Equipment _CreateEquipment(
        string Name,
        float Atk,
        float Def,
        float Value)
    {
        Equipment e = new Equipment();
        e.ID = _equipmentId.ToString();
        _equipmentId += 1;
        e.Name = Name;
        e.Atk = Atk;
        e.Def = Def;
        e.Value = Value;
        equipments.Add(e);
        return e;
    }

    private Team _CreateTeam(string Name, int Score)
    {
        Team t = new Team();
        t.ID = _teamId.ToString();
        _teamId += 1;
        t.Name = Name;
        t.Score = Score;
        teams.Add(t);
        return t;
    }

    public void InitData()
    {
        _CreatePlayer("Dinh0", 25, 100, 100, new List<string> {"0", "9", "7"}, "1");
        _CreatePlayer("Dinh1", 30, 100, 100, new List<string> {"1", "2", "3"}, "1");
        _CreatePlayer("Dinh2", 30, 100, 100, new List<string> {"0", "2", "3"}, "1");
        _CreatePlayer("Dinh3", 30, 100, 100, new List<string> {"5", "8", "3"}, "2");
        _CreatePlayer("Dinh4", 200, 200, 200, new List<string> {"8", "2", "3"}, "2");
        _CreatePlayer("Dinh5", 200, 200, 200, new List<string> {"1", "9", "4"}, "3");
        _CreatePlayer("Dinh6", 50, 50, 100, new List<string> {"1", "7", "4", "6"}, "3");
        _CreatePlayer("Dinh7", 75, 100, 100, new List<string> {"8", "7", "3", "6"}, "1");
        _CreatePlayer("Dinh8", 75, 100, 20, new List<string> {"1", "7", "3"}, "2");
        _CreatePlayer("Dinh9", 25, 50, 20, new List<string> {"2", "9", "7"}, "1");
        _CreatePlayer("Dinh10", 25, 50, 400, new List<string> {"2", "9", "7"}, null);
        _CreatePlayer("Dinh11", 25, 100, 400, new List<string> {"3", "9", "7"}, null);
        _CreatePlayer("Dinh12", 25, 100, 500, new List<string> {"3", "9", "8"}, null);
        _CreatePlayer("Dinh13", 25, 50, 400, new List<string> {"9"}, null);
        _CreatePlayer("Dinh14", 25, 100, 400, new List<string> {"9", "10", "11"}, null);
        _CreatePlayer("Dinh15", 25, 100, 500, new List<string> {"11"}, null);

        _CreateEquipment("glock-18", 15, 5, 2);
        _CreateEquipment("usp-s", 20, 7, 3);
        _CreateEquipment("m4a1-s", 33, 11, 29);
        _CreateEquipment("ak47", 40, 13, 27);
        _CreateEquipment("m4a4", 24, 8, 31);
        _CreateEquipment("smoke", 1, 150, 4);
        _CreateEquipment("flash", 1, 100, 2);
        _CreateEquipment("grenade", 100, 50, 3);
        _CreateEquipment("awp", 120, 40, 47);
        _CreateEquipment("unk_1", 120, 40, 47);
        _CreateEquipment("unk_2", 120, 40, 47);
        _CreateEquipment("unk_3", 120, 40, 47);

        _CreateTeam("ABC", 50);
        _CreateTeam("DEF", 100);
        _CreateTeam("XYZ", 75);
    }

    [ContextMenu("CountPlayersWithTeam")]
    public void CountPlayersWithTeam()
    {
        Debug.Log(LINQTest_Questions.CountPlayersWithTeam(players));
    }

    [ContextMenu("CountTotalEquipmentsEquipped")]
    public void CountTotalEquipmentsEquipped()
    {
        Debug.Log(LINQTest_Questions.CountTotalEquipmentsEquipped(players));
    }

    [ContextMenu("CountDistinctEquipmentsEquipped")]
    public void CountDistinctEquipmentsEquipped()
    {
        Debug.Log(LINQTest_Questions.CountDistinctEquipmentsEquipped(players));
    }

    [ContextMenu("NameOfMostCommonlyEquippedEquipment")]
    public void NameOfMostCommonlyEquippedEquipment()
    {
        Debug.Log(LINQTest_Questions.NameOfMostCommonlyEquippedEquipment(equipments, players));
    }

    [ContextMenu("IsThereAnyEquipmentThatIsNotEquippedByAnyone")]
    public void IsThereAnyEquipmentThatIsNotEquippedByAnyone()
    {
        Debug.Log(LINQTest_Questions.IsThereAnyEquipmentThatIsNotEquippedByAnyone(equipments, players));
    }

    [ContextMenu("CountTeamsWithMoreThanNPlayers")]
    public void CountTeamsWithMoreThanNPlayers(int n)
    {
        Debug.Log(LINQTest_Questions.CountTeamsWithMoreThanNPlayers(teams, players, n));
    }

    [ContextMenu("NamesOfTopNTeamsByScoreDescending")]
    public void NamesOfTopNTeamsByScoreDescending(int n)
    {
        Debug.Log(LINQTest_Questions.NamesOfTopNTeamsByScoreDescending(teams, n));
    }

    [ContextMenu("ScoreDifferenceBetweenTopAndBottomTeam")]
    public void ScoreDifferenceBetweenTopAndBottomTeam()
    {
        Debug.Log(LINQTest_Questions.ScoreDifferenceBetweenTopAndBottomTeam(teams));
    }

    [ContextMenu("SortPlayersByMaxHPDescThenHpDesc")]
    public void SortPlayersByMaxHPDescThenHpDesc()
    {
        var list = LINQTest_Questions.SortPlayersByMaxHPDescThenHpDesc(players).ToList();
        foreach (var p in list)
        {
            Debug.Log(p.ID + " " + p.MaxHP + " " + p.HP);
        }
    }

    [ContextMenu("AverageMissingHP")]
    public void AverageMissingHP()
    {
        Debug.Log(LINQTest_Questions.AverageMissingHP(players));
    }    
    
    [ContextMenu("AverageEquipmentAtk")]
    public void AverageEquipmentAtk()
    {
        Debug.Log(LINQTest_Questions.AverageEquipmentAtk(equipments));
    }   
    
    [ContextMenu("AverageEquippedEquipmentsAtk")]
    public void AverageEquippedEquipmentsAtk()
    {
        Debug.Log(LINQTest_Questions.AverageEquippedEquipmentsAtk(equipments, players));
    }    
    
    [ContextMenu("EquipmentsEquippedByMoreThan1Players")]
    public void EquipmentsEquippedByMoreThan1Players()
    {
        var list = LINQTest_Questions.EquipmentsEquippedByMoreThan1Players2(equipments, players);
        foreach (var element in list)
        {
            Debug.Log(element.Key.Name + " ====================================");
            foreach (var player in element.Value)
            {
                Debug.Log("player id " + player.ID + " name " + player.Name);
            }
        }
    }    
    
    [ContextMenu("EquipmentsEquippedByMoreThan1Players_Spd")]
    public void EquipmentsEquippedByMoreThan1Players2()
    {
        int c = 1000;
        var sw = Stopwatch.StartNew();
        for (int i = 0; i < c; i++)
        {
            LINQTest_Questions.EquipmentsEquippedByMoreThan1Players(equipments, players);
        }
        sw.Stop();
        Debug.Log($"1 took {sw.ElapsedMilliseconds}ms");
        
        sw.Restart();
        for (int i = 0; i < c; i++)
        {
            LINQTest_Questions.EquipmentsEquippedByMoreThan1Players2(equipments, players);
        }
        sw.Stop();
        Debug.Log($"2 took {sw.ElapsedMilliseconds}ms");
    }
    
    // added
    public string GetTeamNameById(string teamId)
    {
        return teams.FirstOrDefault((team) => team.ID == teamId)?.Name;
    }
}