﻿using System.Collections.Generic;
using JetBrains.Annotations;

public class Player
{
    public string ID { get; set; } // ID of player
    public string Name { get; set; } // Name of player
    public float HP { get; set; } // Current HP (0->MaxHP)
    public float MaxHP { get; set; }
    public float Cash { get; set; } // How much cash player has
    public List<string> Equipments { get; } = new List<string>(); // IDs of Equipments
    public string Team { get; set; } // ID of team player is in
}
public class Team
{
    public string ID { get; set; } // ID of team
    public string Name { get; set; } // Name of team
    public int Score { get; set; } // Score of team
}

public class Equipment
{
    public string ID { get; set; } // ID of Equipment
    public string Name { get; set; } // Name of Equipment
    public float Atk { get; set; } // Attack 
    public float Def { get; set; } // Defense
    public float Value { get; set; } // Value of this equipment (like money)
}
