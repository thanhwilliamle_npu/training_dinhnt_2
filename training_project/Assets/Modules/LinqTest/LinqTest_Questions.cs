﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.PlayerLoop;

public class LINQTest_Questions
{
    /*
    Welcome to LINQTest!

    You will have a dozen Teams, a few hundred Players, and a few thousand Equipments.
    Each Player can join 1 team
    Each Player can hold a few equipments

    Look at LinqTest_Data.cs to see what info each object has.
    Finish the functions in the QUESTIONS region. Each function is a question.

    Good luck!
    */

    public static int CountPlayersWithTeam(List<Player> players) // if a player has Team string == null then that player has not joined a team
    { 
        return players.Where((p) => p.Team != null).Count();
    }

    public static int CountTotalEquipmentsEquipped(List<Player> players)
    {
        return players.Sum((p) => p.Equipments.Count);
    }

    public static int CountDistinctEquipmentsEquipped(List<Player> players)
    {
        return players.SelectMany(p => p.Equipments) 
            .Distinct()
            .Count();
    }

    public static string NameOfMostCommonlyEquippedEquipment(List<Equipment> equipments, List<Player> players)
    {
        var key = players?.SelectMany(p => p.Equipments)
            .GroupBy(eq => eq)
            .OrderByDescending(g => g.Count())
            .FirstOrDefault()?.Key;
        var first = equipments
            .FirstOrDefault(equipment => equipment.ID == key);
        return first?.Name;
    }

    public static bool IsThereAnyEquipmentThatIsNotEquippedByAnyone(List<Equipment> equipments, List<Player> players)
    {
        // var queryUsed = players.SelectMany(p => p.Equipments);
        // var queryNotUsed = equipments
        //     .Select(equipment => equipment.ID)
        //     .Except(queryUsed.ToList());
        // return queryNotUsed.Any();

        return equipments.Any(e => !players.Any(p => p.Equipments.Contains(e.ID)));
    }

    public static int CountTeamsWithMoreThanNPlayers(List<Team> teams, List<Player> players, int n)
    {
        var query = teams.GroupJoin(
            players,
            (team) => team.ID,
            (player) => player.Team,
            (team, playerList) => new
            {
                PlayerList = playerList
            }
        );

        var queryCount = query.Count((item) => item.PlayerList.Count() > n);
        return queryCount;
    }

    public static List<string> NamesOfTopNTeamsByScoreDescending(List<Team> teams, int n)
    {
        return teams.OrderByDescending(team => team.Score)
            .Select((team) => team.Name)
            .Take(n)
            .ToList();
    }

    public static float ScoreDifferenceBetweenTopAndBottomTeam(List<Team> teams)
    {
        var list = teams.OrderBy((team) => team.Score * -1).ToList();
        var count = list.Count;
        if (count <= 1) return 0;
        var topTeam = list[0];
        var botTeam = list[count - 1];
        return topTeam.Score - botTeam.Score;
    }

    public static IEnumerable<Player> SortPlayersByMaxHPDescThenHpDesc(List<Player> players)
    {
        return players
            .OrderBy((p) => p.MaxHP * -1)
            .ThenBy((p) => p.HP * -1);
    }

    public static float AverageMissingHP(List<Player> players)
    {
        return players.Average((player) => player.MaxHP - player.HP);
    }

    public static float AverageEquipmentAtk(List<Equipment> equipment)
    {
        return equipment.Average((eq) => eq.Atk);
    }

    public static float AverageEquippedEquipmentsAtk(List<Equipment> equipment, List<Player> players)
    {
        return players.SelectMany(player => player.Equipments)
            .Join(equipment,
                eqId => eqId,
                eq => eq.ID,
                (eqId, eq) => eq.Atk)
            .Average();
    }


    public static Dictionary<Equipment, List<Player>> EquipmentsEquippedByMoreThan1Players(List<Equipment> equipments, List<Player> players)
    {
        
        var query = players
            .SelectMany(
                p => p.Equipments.Distinct(), 
                (p, equipId) => new { Player = p, Equipment = equipId})
            .GroupBy(pWithEq => pWithEq.Equipment)
            .Where(group => group.Count() > 1);

        var query2 = query.Join(
            equipments,
            eq => eq.Key,
            equipment => equipment.ID,
            (eq, equipment) =>
            new
            {
                EquipmentData = equipment,
                PlayerList = eq
            }
        );

        var dict = query2.ToDictionary(
            (element) => element.EquipmentData, 
            (element) => element.PlayerList
                .Select(pWithEq => pWithEq.Player)
                .ToList()
            );
        return dict;
    }

    public static Dictionary<Equipment, List<Player>> EquipmentsEquippedByMoreThan1Players2(List<Equipment> equipments,
        List<Player> players)
    {
        return players.SelectMany(p => p.Equipments.Select(e => (e, p)))
            .GroupBy(pair => pair.e)
            .Where(g=>g.Skip(1).Any())
            .ToDictionary(g => (equipments.Find(eq=>eq.ID==g.Key)),
                g=>g.Select(pp=>pp.p).ToList());
    }

}
