﻿using System;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;

public class GuiTeamInfo : BasePopup
{
    [SerializeField] private Text teamName;
    [SerializeField] private Text teamDetail;
    [SerializeField] private Text playerCount;

    [SerializeField] private GameObject fog;
    [SerializeField] private GameObject baseUI;
    
    [SerializeField] private PlayerCell prefab;
    [SerializeField] private ScrollRect scrollRect;
    private VisibleTeam vTeam;
    
    public void Start()
    {
        FxInDoFadeRawImage(fog);
        FxInDoMove(baseUI);
    }

    public void ShowTeamInfo(VisibleTeam visibleTeam)
    {
        vTeam = visibleTeam;
        teamName.text = visibleTeam.team.Name;
        teamDetail.text = "#" + visibleTeam.rank + " - Score: " + visibleTeam.team.Score;
        playerCount.text = visibleTeam.players.Count() + "";
        ShowPlayerList(visibleTeam.players);
    }

    public void ShowPlayerList(IEnumerable<Player> players)
    {
        foreach (var vP in players)
        {
            PlayerCell a = Instantiate(prefab, scrollRect.content.transform);
            a.SetData(new VisualizePlayer()
            {
                player = vP,
                team = vTeam.team
            });
        }
    }

    public void Close()
    {
        Destroy(gameObject);
    }
}