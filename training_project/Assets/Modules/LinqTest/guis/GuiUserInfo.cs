﻿using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class GuiUserInfo : BasePopup
{
    private VisualizePlayer vP;
    
    [SerializeField] private Text playerNameText;
    [SerializeField] private Text teamText;
    [SerializeField] private Text hpText;
    [SerializeField] private Text cashText;

    [SerializeField] private GameObject fog;
    [SerializeField] private GameObject baseUI;

    public void Start()
    {
        FxInDoFadeRawImage(fog);
        FxInDoMove(baseUI);
    }

    public void ShowUserInfo(VisualizePlayer vP)
    {
        this.vP = vP;
        playerNameText.text = "Player: " + vP.player.Name;
        teamText.text = vP.team != null ? ("Team: " + vP.team.Name) : "";
        hpText.text = vP.player.HP + "/" + vP.player.MaxHP;
        cashText.text = vP.player.Cash.ToString();
    }
    
    public void OnClickTeam()
    {
        if (vP.team == null) return;
        VisibleTeam visibleTeam = VisibleTeam.GetVisibleTeam(vP.team.ID, MainGui.Instance.linq);
        PopupController.Instance.ShowTeamInfo(visibleTeam);
    }

    public void Close()
    {
        Destroy(gameObject);
    }
}
