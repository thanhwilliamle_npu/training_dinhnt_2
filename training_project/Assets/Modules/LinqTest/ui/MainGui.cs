﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainGui : MonoBehaviour
{
    private static MainGui _instance;
    public static MainGui Instance => _instance;
    private void Awake()
    {
        _instance = this;
    }

    public LinqTest linq = new LinqTest();
    private string[] _tabNames = {"Cv_Players"};
    [SerializeField] private GameObject[] _tabs;
    [SerializeField] private Button[] _btns;
    
    void Start()
    {
        linq.InitData();
        
        GameObject teamTab = _ShowTabByName("Cv_Teams");
        teamTab.GetComponent<TeamList>().SaveList(linq);
        teamTab.GetComponent<TeamList>().ReloadTable();
        
        GameObject playerTab = _ShowTabByName("Cv_Players");
        _ShowButtonFocusByName("Cv_Players");
        playerTab.GetComponent<PlayerList>().SaveList(linq);
        playerTab.GetComponent<PlayerList>().ReloadTable();
    }

    public void FocusPlayers()
    {
        _ShowTabByName("Cv_Players");
        _ShowButtonFocusByName("Cv_Players");
    }

    public void FocusTeams()
    {
        _ShowTabByName("Cv_Teams");
        _ShowButtonFocusByName("Cv_Teams");
    }
    
    private GameObject _ShowTabByName(string shownTabName)
    {
        GameObject returnObj = null;
        foreach (var tab in _tabs)
        {
            tab.SetActive(false);
            if (tab.GetComponent<TabDistinguish>().tabId == shownTabName)
            {
                returnObj = tab;
                tab.SetActive(true);
            }
        }
        return returnObj;
    }

    private void _ShowButtonFocusByName(string shownTabName)
    {
        foreach (var btn in _btns)
        {
            btn.targetGraphic.color = Color.white;
            if (btn.GetComponent<TabDistinguish>().tabId == shownTabName)
                btn.targetGraphic.color = Color.green;
        }
    }
}
