﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;

public struct VisualizePlayer
{
    public Player player;
    public Team team;
    public IEnumerable<Equipment> equipments;

    public static IEnumerable<VisualizePlayer> CreateListFromLinq(LinqTest linq)
    {
        var list = linq.players.Select(p =>
        {
            VisualizePlayer vP;
            vP.player = p;
            vP.team = linq.teams.Find((t) => t.ID == p.Team);
            vP.equipments = linq.equipments
                .Where((e) => p.Equipments.Find((eID) => eID == e.ID) != null)
                .ToList();
            return vP;
        }).ToList();
        return list;
    }
}

public class PlayerList : MonoBehaviour
{
    private IEnumerable<VisualizePlayer> list;
    [SerializeField] public PlayerCell playerCellPrefab;
    [SerializeField] private ScrollRect scrollRect;

    public void SaveList(LinqTest linq)
    {
        list = VisualizePlayer.CreateListFromLinq(linq);
    }
    
    public void SortByName()
    {
        list = list.OrderBy((vP) => vP.player.Name).ToList();
        ReloadTable();
    }

    public void SortByCash()
    {
        list = list.OrderByDescending((vP) => vP.player.Cash).ToList();
        ReloadTable();
    }    
    
    public void SortCustom1()
    {
        list = list
            .OrderBy((vP) => vP.team != null ? - vP.team.Score : Mathf.Infinity)
            .ThenByDescending((vP) => vP.player.Cash)
            .ToList();
        ReloadTable();
    }
    
    public void ReloadTable()
    {
        Transform scrollViewContent = scrollRect.content;
        foreach (Transform child in scrollViewContent.transform) {
            Destroy(child.gameObject);
        }

        foreach (var vP in list)
        {
            PlayerCell a = Instantiate(playerCellPrefab, scrollViewContent.transform);
            a.GetComponent<PlayerCell>().SetData(vP);
        }
    }
}
