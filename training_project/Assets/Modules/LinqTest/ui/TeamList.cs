﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public struct VisibleTeam
{
    public Team team;
    public IEnumerable<Player> players;
    public int rank;

    public static VisibleTeam GetVisibleTeam(string TeamId, LinqTest linq)
    {
        var query = linq.teams
            .OrderByDescending((t) => t.Score)
            .GroupJoin(
                linq.players,
                (team) => team.ID,
                (player) => player.Team,
                (team, playerList) => new
                {
                    Team = team,
                    PlayerList = playerList.ToList()
                });

        VisibleTeam returnTeam = query.Select((t, index) =>
        {
            VisibleTeam team;
            team.team = t.Team;
            team.rank = index + 1;
            team.players = t.PlayerList;
            return team;
        }).ToList().Find((vTeam) => vTeam.team.ID == TeamId);

        return returnTeam;
    }
}

public class TeamList : MonoBehaviour
{
    private IEnumerable<VisibleTeam> teamList;
    [SerializeField] private TeamCell teamCellPrefab;
    [SerializeField] private ScrollRect scrollRect;
    
    public void SaveList(LinqTest linq)
    {
        var query = linq.teams
            .OrderByDescending((t) => t.Score)
            .GroupJoin(
            linq.players,
            (team) => team.ID,
            (player) => player.Team,
            (team, playerList) => new
            {
                Team = team,
                PlayerList = playerList.ToList()
            });

        teamList = query.Select((t, index) =>
        {
            VisibleTeam team;
            team.team = t.Team;
            team.rank = index + 1;
            team.players = t.PlayerList;
            return team;
        });
    }

    public void SortByName()
    {
        teamList = teamList.OrderBy((vTeam) => vTeam.team.Name).ToList();
        ReloadTable();
    }

    public void SortByScore()
    {
        teamList = teamList.OrderByDescending((vTeam) => vTeam.team.Score).ToList();
        ReloadTable();
    }    
    
    public void SortByPlayerCount()
    {
        teamList = teamList.OrderByDescending((vTeam) => vTeam.players.Count()).ToList();
        ReloadTable();
    }
    
    public void ReloadTable()
    {
        Transform scrollViewContent = scrollRect.content;
        foreach (Transform child in scrollViewContent.transform) {
            Destroy(child.gameObject);
        }
        
        for (int i = 0; i < teamList.Count(); i += 1)
        {
            TeamCell a = Instantiate(teamCellPrefab, scrollViewContent.transform);
            var data = teamList.ElementAt(i);
            a.GetComponent<TeamCell>().SetData(data);
        }
    }
}
