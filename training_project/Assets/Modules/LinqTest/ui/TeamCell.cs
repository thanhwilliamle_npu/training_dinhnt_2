﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;

public class TeamCell : MonoBehaviour
{
    [SerializeField] private Text textRanking;
    [SerializeField] private Text textScore;
    [SerializeField] private Text textTeamName;
    [SerializeField] private Text textPlayerCount;
    
    public void SetData(VisibleTeam vTeam)
    {
        textRanking.text = "#" + vTeam.rank;
        textScore.text = "Score: " + vTeam.team.Score.ToString();
        textTeamName.text = vTeam.team.Name;
        textPlayerCount.text = "Player Num: " + vTeam.players.Count();
    }
}
