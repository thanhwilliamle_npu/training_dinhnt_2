﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerCell : MonoBehaviour
{
    private VisualizePlayer vP;

    [SerializeField] private bool showTeamName;
    [SerializeField] private Text playerNameText;
    [SerializeField] private Text hpText;
    [SerializeField] private Text cashText;

    public void SetData(VisualizePlayer vP)
    {
        this.vP = vP;
        playerNameText.text = vP.player.Name + " " + (showTeamName && vP.team != null ? " (" + vP.team.Name + ")" : "");
        hpText.text = vP.player.HP + "/" + vP.player.MaxHP;
        cashText.text = vP.player.Cash.ToString();
    }

    public void ShowPopup()
    {
        PopupController.Instance.ShowGuiUserInfo(vP);
    }
}
