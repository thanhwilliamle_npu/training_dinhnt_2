﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "HAHA", menuName = "Dinh/PLEASE MAKE NEW PLAYER CONFIG")]
public class PlayerConfig : ScriptableObject
{
    [SerializeField] float maxHP = 10;

    public float MaxHP
    {
        get => maxHP;
    }
}
