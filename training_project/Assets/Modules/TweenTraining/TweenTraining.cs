﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class TweenTraining : MonoBehaviour
{
    [SerializeField] private Vector3 posA, posB, posC, posD;
    [SerializeField] private Vector3 scaleA, scaleB;
    [SerializeField] private Vector3 forwardA, forwardB;
    [SerializeField] private Ease ease;
    [SerializeField] private AnimationCurve easeCurve;
    [SerializeField] private bool useEaseCurve;

    private float _timeElapsed;
    private bool _started;

    public GameObject text3DPrefab;
    public Material lineRendererMaterial;
    
    // Start is called before the first frame update
    void Start()
    {
        _timeElapsed = 0;
        _started = false;
        
        _drawPosition(posA, "A");
        _drawPosition(posB, "B");
        _drawPosition(posC, "C");
        _drawPosition(posD, "D");
    }
#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        Gizmos.DrawSphere(posA, 0.2f);
        UnityEditor.Handles.Label(posA + Vector3.down, "Point A");
        Gizmos.DrawSphere(posB, 0.2f);
        UnityEditor.Handles.Label(posB + Vector3.down, "Point B");
        Gizmos.DrawSphere(posC, 0.2f);
        UnityEditor.Handles.Label(posC + Vector3.down, "Point C");
        Gizmos.DrawSphere(posD, 0.2f);
        UnityEditor.Handles.Label(posD + Vector3.down, "Point D");

        var trans = transform;
        var position = trans.position;
        Gizmos.color = Color.blue;
        Gizmos.DrawLine(position, position + trans.forward);
        
        Gizmos.color = Color.red;
        Gizmos.DrawLine(position, position + forwardB);
    }
#endif
    // void Update()
    // {
    //     _timeElapsed += Time.deltaTime;
    //     if (_started) return;
    //     if (_timeElapsed > 2)
    //     {
    //         // _moveFromAToB();
    //         // _moveToC();
    //         // _movePathABCD();
    //         // _moveABAndReverseInfinitely();
    //         // _scaleAtoB();
    //         // _rotateToB();
    //         // _customMovement1();
    //         // _customMovement2();
    //         _customMovement3();
    //         
    //         // transform.forward = transform.TransformDirection(Vector3.forward)
    //         
    //         _started = true;
    //     }
    // }

    private Tween _setEaseForAction(Tween action)
    {
        if (useEaseCurve) return action.SetEase(easeCurve);
        else return action.SetEase(ease);
    }
    private void _drawPosition(Vector3 position, String posName)
    {
        GameObject visualizePosition = new GameObject();
        
        // line renderer
        visualizePosition.AddComponent<LineRenderer>();
        var lineRenderer = visualizePosition.GetComponent<LineRenderer>();
        lineRenderer.startColor = Color.cyan;
        lineRenderer.endColor = Color.cyan;
        lineRenderer.material = lineRendererMaterial;
        var width = 0.5f;
        var alignVector = new Vector3(width/2, 0, 0);
        lineRenderer.SetWidth(width, width);
        lineRenderer.SetPosition(0, position - alignVector);
        lineRenderer.SetPosition(1, position + alignVector);
        
        GameObject textObj = Instantiate(text3DPrefab, position - 4 * alignVector, Quaternion.identity);
        var textComponent = textObj.GetComponent<TextMesh>();
        textComponent.text = posName;
    }   

    [ContextMenu("_stopAllTweens")]
    private void _stopAllTweens()
    {
        DOTween.KillAll();
    }

    [ContextMenu("_moveFromAToB")]
    private void _moveFromAToB()
    {
        transform.position = posA;
        _setEaseForAction(transform.DOMove(posB, 1));
    }

    [ContextMenu("_moveToC")]
    private void _moveToC()
    {
        _setEaseForAction(transform.DOMove(posC, 1));
    }

    [ContextMenu("_movePathABCD")]
    private void _movePathABCD()
    {
        var path = new Vector3[4];
        path = new[] {posA, posB, posC, posD};
        _setEaseForAction(transform.DOPath(path, 4));
    }

    [ContextMenu("_moveABAndReverseInfinitely")]
    private void _moveABAndReverseInfinitely()
    {
        transform.position = posA;
        Sequence mySeq = DOTween.Sequence();
        mySeq.Append(_setEaseForAction(transform.DOMove(posB, 1)));
        mySeq.Append(_setEaseForAction(transform.DOMove(posA, 1)));
        mySeq.SetLoops(-1);
    }

    [ContextMenu("_scaleAtoB")]
    private void _scaleAtoB()
    {
        transform.localScale = scaleA;
        _setEaseForAction(transform.DOScale(scaleB, 1));
    }

    [ContextMenu("_rotateToB")]
    private void _rotateToB()
    {
        Quaternion rotation = Quaternion.LookRotation(forwardB);
        _setEaseForAction(transform.DORotate(rotation.eulerAngles, 1 ));
    }

    [ContextMenu("_customMovement1")]
    private void _customMovement1()
    {
        
        Sequence mySeq = DOTween.Sequence();
        mySeq.AppendCallback(() =>
        {
            var trans = transform;
            trans.position = posA;
            trans.localScale = scaleA;
        });
        mySeq.Append(_setEaseForAction(transform.DOMove(posC, 1)));
        mySeq.Append(_setEaseForAction(transform.DOScale(scaleB, 1)));
        mySeq.Append(_setEaseForAction(transform.DOMove(posD, 1)));
    }

    [ContextMenu("_customMovement2")]
    private void _customMovement2()
    {
        Sequence mySeq = DOTween.Sequence();
        mySeq.AppendCallback(() =>
        {
            var trans = transform;
            trans.position = posA;
            trans.localScale = scaleA;
        });
        mySeq.Append(_setEaseForAction(transform.DOMove(posC, 1)));
        mySeq.Append(_setEaseForAction(transform.DOScale(scaleB, 1)));
        mySeq.Append(_setEaseForAction(transform.DOMove(posD, 1)));
        mySeq.SetLoops(-1);
    }

    [ContextMenu("_customMovement3")]
    private void _customMovement3()
    {
        Sequence mySeq = DOTween.Sequence();
        mySeq.AppendCallback(() =>
        {
            var trans = transform;
            trans.position = posA;
            trans.localScale = scaleA;
        });
        mySeq.Append(_setEaseForAction(transform.DOScale(scaleB, 1)));
        mySeq.Join(_setEaseForAction(transform.DOMove(posC, 1)));
        mySeq.AppendInterval(1);
        mySeq.Append(_setEaseForAction(transform.DOMove(posD, 1)));
        mySeq.SetLoops(3);
    }
}
