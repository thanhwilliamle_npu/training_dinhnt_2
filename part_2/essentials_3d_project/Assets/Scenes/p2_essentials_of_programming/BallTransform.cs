﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallTransform : MonoBehaviour
{
    private Vector3 scaleChange; // per second
    private Vector3 rotationChange; // per second
    private Vector3 positionChange;
    
    // Start is called before the first frame update
    void Start()
    {
        scaleChange = new Vector3(1.2f, 1.2f, 1.2f);
        rotationChange = new Vector3(0, 45, 0);
        positionChange = new Vector3(0, 2f, 0);
    }

    // Update is called once per frame
    void Update()
    {
        transform.localScale += scaleChange * Time.deltaTime;
        transform.Rotate(rotationChange * Time.deltaTime);
        transform.position += positionChange * Time.deltaTime;
    }
}
