﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GuiLobbyScript : MonoBehaviour
{
    [SerializeField] private Text level;
    
    void Awake()
    {
        var mgr = GlobalManager.Instance.LevelManager;
        var curLevel = mgr.CurrentLevel;
        level.text = "Current Level: " + GlobalManager.Instance.LevelManager.LevelDisplay(curLevel);
        GamePlayManager.Instance.PrepareNewGame();
    }

    public void OnClickPlayNow()
    {
        GamePlayManager.Instance.StartGame();
        Destroy(gameObject);
    }
    
    public void OnClickBtnShop()
    {
        GuiManager.Instance.ShowShop();
    }
}
