﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class NodeMoney : MonoBehaviour
{
    private int curValue;
    [SerializeField] private Text gold;
    [SerializeField] private Text goldChange;

    void Awake()
    {
        _ResetPositionFxGold();
        _ShowGold();
        curValue = UserData.Gold;
        UserData.GoldChange += _OnGoldChange;
    }

    private void OnDestroy()
    {
        UserData.GoldChange -= _OnGoldChange;
    }

    private void _OnGoldChange()
    {
        _EffectChangeGold();
        _ShowGold();
    }
    private void _ShowGold()
    {
        gold.text = "$" + UserData.Gold;
    }

    private void _EffectChangeGold()
    {
        _ResetPositionFxGold();
        goldChange.transform
            .DOMove(Vector2.down * 96, 0.3f)
            .SetEase(Ease.OutQuart)
            .SetRelative();

        var seq = DOTween.Sequence();
        seq.SetId(goldChange);
        seq.Append(goldChange.DOFade(1, 0.3f));
        seq.AppendInterval(3);
        seq.Append(goldChange.DOFade(0, 0.3f));

        var newGold = UserData.Gold;
        if (newGold > curValue)
        {
            goldChange.text = "+ $" + (newGold - curValue);
            goldChange.color = Color.green;
        }
        else
        {
            goldChange.text = "- $" + (curValue - newGold);
            goldChange.color = Color.red;
        }

        curValue = newGold;
    }
    private void _ResetPositionFxGold()
    {
        DOTween.Kill(goldChange);
        DOTween.Kill(goldChange.transform);
        goldChange.transform.position = gold.transform.position;
        goldChange.color = new Color(0, 0, 0, 0);
    }
}
