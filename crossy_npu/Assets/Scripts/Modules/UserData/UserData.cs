﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserData
{
    public static int Gold
    {
        get => PlayerPrefs.GetInt("PlayerGold", 0);
        set
        {
            PlayerPrefs.SetInt("PlayerGold", value);
            GoldChange.Invoke();
        }
    }

    public static Action GoldChange;
}
