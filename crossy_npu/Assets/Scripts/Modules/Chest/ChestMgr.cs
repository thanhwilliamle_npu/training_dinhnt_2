﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public struct ChestReward
{
    public SkinData SkinData;
    public int gold;
}

[CreateAssetMenu(menuName = "Chest/ChestMgr")]
public class ChestMgr : ScriptableObject
{
    [SerializeField] public int progToOpenChest;
    [SerializeField] public int progAddPerGame;
    [SerializeField] public int minGold;
    [SerializeField] public int maxGold;

    public void Init()
    {
        GamePlayManager.Instance.OnGameWin += _OnGameWon;
    }

    public int GetProgressChest()
    {
        return PlayerPrefs.GetInt("ProgressChest", 0);
    }

    private void _OnGameWon()
    {
        _AddProgressChest(progAddPerGame);
    }

    private void _AddProgressChest(int prog)
    {
        var curValue = GetProgressChest();
        curValue += prog;
        PlayerPrefs.SetInt("ProgressChest", curValue);
    }

    public ChestReward OpenOneChest()
    {
        _AddProgressChest(-progToOpenChest);
        ChestReward reward;
        
        // select skin
        var list = GlobalManager.Instance.SkinManager.ListCharacterNotOwned();
        var idx = Mathf.Floor(Random.Range(0 - 0.49f, list.Count - 1 + 0.49f));
        var data = list[Mathf.RoundToInt(idx)];
        data.Unlocked = true;
        data.Owned = true;
        reward.SkinData = data;
        
        // select gold
        reward.gold = Mathf.RoundToInt(Random.Range(minGold - 0.49f, maxGold + 0.49f));
        UserData.Gold += reward.gold;
        
        return reward;
    }
}
