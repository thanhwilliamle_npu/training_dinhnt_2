﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class CellShop : MonoBehaviour
{
    [SerializeField] private GameObject bgStatusSelected;
    [SerializeField] private Image avatarRenderer;
    [SerializeField] private GameObject bgNotUnlocked;
    [SerializeField] private Button btnSelect;
    [SerializeField] private Button btnBuy;
    [SerializeField] private Text textUnlockAtLevel;
    [SerializeField] private Text textUnlockFromChest;
    [SerializeField] private Text skinName;
    [SerializeField] private Text textPrice;
    
    private SkinData _skinData;
    public void SetSkinData(SkinData data)
    {
        _skinData = data;
        _PresentData();
    }
    private void _PresentData()
    {
        SkinManager mgr = GlobalManager.Instance.SkinManager;
        var skinData = _skinData;

        avatarRenderer.sprite = skinData.icon;
        skinName.text = skinData.skinName;
        textPrice.text = "Buy\n" + skinData.price;
        textUnlockAtLevel.gameObject.SetActive(false);

        var curSkinData = mgr.CurrentSkinData();
        bool selected = curSkinData.id == skinData.id;
        
        bool owned = skinData.Owned;
        bool unlocked = skinData.Unlocked;
        
        bgNotUnlocked.SetActive(!unlocked);
        bgStatusSelected.SetActive(selected);
        btnBuy.gameObject.SetActive(!owned && unlocked);
        btnSelect.gameObject.SetActive(owned && !selected);
        textUnlockAtLevel.gameObject.SetActive(!unlocked && skinData.unlockMethod == SkinUnlockMethod.COMPLETE_LEVEL);
        textUnlockAtLevel.text = "Unlock at level " + GlobalManager.Instance.LevelManager.LevelDisplay(skinData.unlockLevel);
        textUnlockFromChest.gameObject.SetActive(!unlocked && skinData.unlockMethod == SkinUnlockMethod.OPEN_CHEST);
    }

    public void OnClickSelect()
    {
        SkinManager mgr = GlobalManager.Instance.SkinManager;
        mgr.CurrentSkinId = _skinData.id;
    }    
    
    public void OnClickBuySkin()
    {
        if (_skinData.price > UserData.Gold)
            GuiManager.Instance.ShowNotEnoughMoney();
        else
        {
            UserData.Gold -= _skinData.price;
            _skinData.Owned = true;
            _PresentData();
        }
    }
}
