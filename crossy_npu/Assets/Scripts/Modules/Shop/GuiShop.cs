﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuiShop : MonoBehaviour
{
    [SerializeField] private GameObject tableContent; 
    [SerializeField] private CellShop cellShopPrefab;

    private void Awake()
    {
        _LoadShopList();
        
        var skinManager = GlobalManager.Instance.SkinManager;
        skinManager.OnChangeSkin -= _OnSkinChange;
        skinManager.OnChangeSkin += _OnSkinChange;
    }

    private void OnDestroy()
    {
        var skinManager = GlobalManager.Instance.SkinManager;
        skinManager.OnChangeSkin -= _OnSkinChange;
    }

    private void _OnSkinChange()
    {
        _LoadShopList();
    }
    
    private void _LoadShopList()
    {
        foreach (Transform child in tableContent.transform)
        {
            Destroy(child.gameObject);
        }

        var skinLists = GlobalManager.Instance.SkinManager.AllSkins;
        for (int i = 0; i < skinLists.Count; i += 1)
        {
            CellShop cell = Instantiate(cellShopPrefab, tableContent.transform);
            cell.SetSkinData(skinLists[i]);
        }
    }

    public void OnClickButtonBack()
    {
        Destroy(gameObject);
    }
}
