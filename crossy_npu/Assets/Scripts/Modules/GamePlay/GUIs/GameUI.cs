﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameUI : MonoBehaviour
{
    public void OnClickWin()
    {
        GamePlayManager.Instance.GameWin();
    }
    
    public void OnClickLose()
    {
        GamePlayManager.Instance.GameLose();
    }
}
