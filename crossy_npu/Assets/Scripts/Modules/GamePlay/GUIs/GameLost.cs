﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class GameLost : MonoBehaviour
{
    [SerializeField] private Button btn2ndChance;
    [SerializeField] private Text chanceText;
    [SerializeField] private float chance;

    private void Awake()
    {
        float random = Random.Range(0, 100);
        btn2ndChance.gameObject.SetActive(random <= chance);
        chanceText.text = "(" + chance + "% chance to show this button)";
    }

    public void OnClickTryAgain()
    {
        GuiManager.Instance.ShowLobby();
        Destroy(gameObject);
    }
    
    public void OnClick2ndChance()
    {
        GamePlayManager.Instance.Revive();
        Destroy(gameObject);
    }
}
