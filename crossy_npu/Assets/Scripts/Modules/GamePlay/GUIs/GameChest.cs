﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class GameChest : MonoBehaviour
{
    [SerializeField] private Button btnOpenChest;
    [SerializeField] private Button btnContinue;
    [SerializeField] private GameObject chest;
    [SerializeField] private GameObject chestContainer;
    [SerializeField] private GameObject character;

    private void Start()
    {
        _DoAppearGui();
    }

    private void _DoAppearGui()
    {
        btnOpenChest.gameObject.SetActive(true);
        btnContinue.gameObject.SetActive(false);

        var chestTransform = chest.transform;
        var curPos = chestTransform.position;
        chestTransform.DOMove(curPos + Vector3.up * 1000, 1.2f)
            .From()
            .SetEase(Ease.OutQuart);
        chestTransform.DOLocalRotate(new Vector3(0, 0, 90), 0.6f)
            .SetEase(Ease.Linear)
            .SetLoops(-1, LoopType.Incremental)
            .SetRelative();
    }

    private void _OpenChest()
    {
        btnOpenChest.gameObject.SetActive(false);
        
        var chestTransform = chest.transform;
        DOTween.Kill(chestTransform);
        chestTransform.DOLocalRotate(new Vector3(0, 0, 0), 0.6f)
            .SetEase(Ease.OutQuart);

        var self = this;
        var chestContainerTrans = chestContainer.transform;
        chestContainerTrans
            .DOShakePosition(1.6f, Vector3.right * 50, 20)
            .OnComplete(() =>
            {
                chestContainerTrans.DOMove(Vector3.up * 2000, 2)
                    .SetEase(Ease.Linear)
                    .SetRelative()
                    .OnComplete(() =>
                    {
                        btnContinue.gameObject.SetActive(true);
                    });

                var mgr = GlobalManager.Instance.ChestMgr;
                var reward = mgr.OpenOneChest();
                var unlockData = reward.SkinData;
                
                var characterPresenter = Instantiate(unlockData.model3D, character.transform);
                self._SetLayerRecursively(characterPresenter, LayerMask.NameToLayer("UI"));

                characterPresenter.transform.DOScale(Vector3.one * 300, 1f);
                characterPresenter.transform.DOLocalRotate(new Vector3(0, 90, 0), 0.6f)
                    .SetEase(Ease.Linear)
                    .SetLoops(-1, LoopType.Incremental)
                    .SetRelative();
            });
    }

    private void _SetLayerRecursively(GameObject obj, int layer)
    {
        obj.layer = layer;
   
        foreach (Transform child in obj.transform)
        {
            _SetLayerRecursively(child.gameObject, layer);
        }
    }
        
    public void OnClickOpenChest()
    {
        _OpenChest();
    }

    public void OnClickContinue()
    {
        Destroy(gameObject);
    }
}
