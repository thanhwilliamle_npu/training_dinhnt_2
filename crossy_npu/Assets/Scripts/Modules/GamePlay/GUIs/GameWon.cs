﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class GameWon : MonoBehaviour
{
    [SerializeField] private Button btnOpenChest;
    [SerializeField] private Button btnContinue;
    [SerializeField] private Text progressChest;

    private void Awake()
    {
        _ShowProgressChestDataOnGui();
    }

    private void _ShowProgressChestDataOnGui()
    {
        var skinMgr = GlobalManager.Instance.SkinManager;
        var chestMgr = GlobalManager.Instance.ChestMgr;
        int curProg = chestMgr.GetProgressChest();
        int progToOpenChest = chestMgr.progToOpenChest;
        bool canOpenChest = curProg >= progToOpenChest && skinMgr.HaveCharacterNotOwned();
        if (curProg >= progToOpenChest) progressChest.text = "100%";
        else progressChest.text = curProg + "%";
        btnContinue.gameObject.SetActive(!canOpenChest);
        btnOpenChest.gameObject.SetActive(canOpenChest);
    }

    public void OnClickContinue()
    {
        GuiManager.Instance.ShowLobby();
        Destroy(gameObject);
    }
    
    public void OnClickOpenChest()
    {
        GuiManager.Instance.ShowLobby();
        GuiManager.Instance.ShowGuiOpenChest();
        Destroy(gameObject);
    }
}
