﻿using System;
using UnityEngine;

public enum MapCubeType
{
    Empty = -1,
    Grass = 0,
    Road = 1,
    Water = 2,
    Block = 3,
    FinishLine = 4,
    Jump = 5,
    Slide = 6,
    Stun = 7,
}

public class MapCube : MonoBehaviour
{
    [SerializeField] public MapCubeType type;
}