﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

public class MapData : MonoBehaviour
{
    [SerializeField] private GameObject cubeContainer;
    [SerializeField] public int leftBound;
    [SerializeField] public int rightBound;
    [SerializeField] private Material squareMaterial;
    [SerializeField] private Material normalMaterial;

    private Dictionary<Vector2Int, MapCube> _logicalCube;

    public Dictionary<Vector2Int, MapCube> LogicalCube
    {
        get
        {
            if (_logicalCube == null)
            {
                _logicalCube = new Dictionary<Vector2Int, MapCube>();
                var childs = cubeContainer.GetComponentsInChildren<MapCube>();
                foreach (var child in childs)
                {
                    var childPosition = child.transform.position;
                    int x = Mathf.RoundToInt(childPosition.x);
                    int z = Mathf.RoundToInt(childPosition.z);

                    _logicalCube[new Vector2Int(x, z)] = child.GetComponent<MapCube>();
                }
            }

            return _logicalCube;
        }
    }

    [SerializeField] private GameObject carFactoryContainer;
    private Dictionary<int, CarFactory> _carFactories;

    public Dictionary<int, CarFactory> CarFactories
    {
        get
        {
            if (_carFactories != null) return _carFactories;
            _carFactories = new Dictionary<int, CarFactory>();
            var carFactories = carFactoryContainer.GetComponentsInChildren<CarFactory>();
            foreach (var child in carFactories)
            {
                _carFactories[Mathf.RoundToInt(child.transform.position.z)] = child;
            }

            return _carFactories;
        }
    }

    [SerializeField] private GameObject boardFactoryContainer;
    private Dictionary<int, BoardFactory> _boardFactories;

    public Dictionary<int, BoardFactory> BoardFactories
    {
        get
        {
            if (_boardFactories != null) return _boardFactories;
            _boardFactories = new Dictionary<int, BoardFactory>();
            var boardFactories = boardFactoryContainer.GetComponentsInChildren<BoardFactory>();
            foreach (var child in boardFactories)
            {
                _boardFactories[Mathf.RoundToInt(child.transform.position.z)] = child;
            }

            return _boardFactories;
        }
    }

    [SerializeField] private GameObject edibleItemContainer;
    private List<EdibleItem> _edibleItemList;

    public List<EdibleItem> EdibleItemList
    {
        get
        {
            if (_edibleItemList != null) return _edibleItemList;
            _edibleItemList = new List<EdibleItem>();
            var edibleItems = edibleItemContainer.GetComponentsInChildren<EdibleItem>();
            foreach (var item in edibleItems)
            {
                _edibleItemList.Add(item);
            }

            return _edibleItemList;
        }
    }

    [NonSerialized] public StartPoint StartPoint;

    public int MapLength
    {
        get => Math.Abs(leftBound - rightBound);
    }

    public int MaxLogicalX
    {
        get => LogicalCube
            .Select((pair) => pair.Key.x)
            .DefaultIfEmpty(0)
            .Max();
    }

    public int MinLogicalX
    {
        get => LogicalCube
            .Select((pair) => pair.Key.x)
            .DefaultIfEmpty(0)
            .Min();
    }

    public int MaxLogicalYCanJump
    {
        get => LogicalCube.Select((pair) => pair.Key.y)
            .DefaultIfEmpty(0)
            .Max();
    }

    public int MinLogicalYCanJump
    {
        get => LogicalCube.Select((pair) => pair.Key.y)
            .DefaultIfEmpty(0)
            .Min();
    }

    public int MinLogicalXCanJump
    {
        get => leftBound + 1;
    }

    public int MaxLogicalXCanJump
    {
        get => rightBound - 1;
    }

    void Awake()
    {
        _SynchronizeChildren();
    }
    
#if UNITY_EDITOR
    public void ClearCurrentData()
    {
        _edibleItemList = null;
        _boardFactories = null;
        _carFactories = null;
        _logicalCube = null;
    }
#endif
    
    private void _UpdateMaterials()
    {
        foreach (var pair in LogicalCube)
        {
            var meshRenderers = pair.Value.GetComponentsInChildren<MeshRenderer>();
            foreach (var mr in meshRenderers)
            {
                if (mr.GetComponent<CanApplyDarknessMegaToon>() == null) continue;

                var targetMaterial = normalMaterial;
                // if (pair.Key.x <= leftBound || pair.Key.x >= rightBound) targetMaterial = squareMaterial;
                if (mr.sharedMaterial != targetMaterial) mr.sharedMaterial = targetMaterial;
            }
        }
    }

    private void _SynchronizeChildren()
    {
        var a = LogicalCube;
        _UpdateMaterials();

        var b = CarFactories;
        var c = BoardFactories;
        var d = EdibleItemList;

        StartPoint = GetComponentInChildren<StartPoint>();
    }

    public bool CanJumpIn(Vector2Int pos)
    {
        if (!LogicalCube.ContainsKey(pos)) return false;
        return pos.x > leftBound && pos.x < rightBound && LogicalCube[pos].type != MapCubeType.Block;
    }

    public bool IsWater(Vector2Int pos)
    {
        return LogicalCube[pos].type == MapCubeType.Water;
    }

    public bool PotentialReviveRow(int logicalY)
    {
        var cube = LogicalCube[new Vector2Int(0, logicalY)].type;
        return cube != MapCubeType.Road && cube != MapCubeType.Water;
    }

    public MapCube GetCubeAtPosition(Vector3 position)
    {
        Vector2Int logical = new Vector2Int();
        logical.x = Mathf.RoundToInt(position.x);
        logical.y = Mathf.RoundToInt(position.z);
        if (LogicalCube.ContainsKey(logical)) return LogicalCube[logical];
        return null;
    }
    
    public EdibleItem GetEdibleItemAtPosition(Vector3 position)
    {
        foreach (var item in EdibleItemList)
        {
            if ((item.transform.position - position).magnitude < 0.0001f) return item;
        }
        return null;
    }
}