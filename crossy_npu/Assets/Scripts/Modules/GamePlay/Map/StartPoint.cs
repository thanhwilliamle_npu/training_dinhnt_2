﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class StartPoint : MonoBehaviour
{
#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        var curPos = gameObject.transform.position;
        curPos.y = 0;
        Handles.DrawSolidRectangleWithOutline(new[]
        {
            curPos + Vector3.up + (Vector3.forward + Vector3.left) * 0.5f,
            curPos + Vector3.up + (Vector3.left + Vector3.back) * 0.5f,
            curPos + Vector3.up + (Vector3.back + Vector3.right) * 0.5f,
            curPos + Vector3.up + (Vector3.right + Vector3.forward) * 0.5f,
        }, Color.yellow, Color.red);
        Handles.Label(curPos + Vector3.up, "Start Here");
    }
#endif

    public Vector3 GetStartPosition()
    {
        var curPos = gameObject.transform.position;
        curPos.y = 1;
        return curPos;
    }
}