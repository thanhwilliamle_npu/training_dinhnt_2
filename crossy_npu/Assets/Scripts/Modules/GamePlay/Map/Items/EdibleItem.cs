﻿using System;
using UnityEngine;

public enum EdibleType
{
    StopAlarm = 1,
    Invisible = 2,
    Reverse = 3,
    Coin = 4
}

public class EdibleItem : MonoBehaviour
{
    [NonSerialized] public Action<EdibleItem> EatItem;
    [SerializeField] public EdibleType type;
    private void OnTriggerEnter(Collider collide)
    {
        var charControl = collide.gameObject.GetComponent<CharacterControl>();
        if (charControl)
        {
            EatItem?.Invoke(this);
            Destroy(gameObject);
        }
    }
}
