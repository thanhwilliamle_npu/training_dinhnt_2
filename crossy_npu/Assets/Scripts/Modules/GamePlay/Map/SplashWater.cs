﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class SplashWater : MonoBehaviour
{
    [SerializeField] public GameObject prefab;
    private GameObject[] _cube;

    private void Start()
    {
        const int length = 10;
        _cube = new GameObject[length];
        for (int i = 0; i < length; i += 1)
        {
            var cube = Instantiate(prefab, transform);
            cube.transform.localPosition = new Vector3(
                Random.Range(-.5f, .5f),
                Random.Range(0, .5f),
                Random.Range(-.5f, .5f)
            );
            _cube[i] = cube;
        }
    }

    public void DoSplash()
    {
        foreach (var cube in _cube)
        {
            DOTween.Kill(cube);
            var curPos = cube.transform.localPosition;
            var topPos = curPos + Vector3.up * 3;
            Sequence mySeq = DOTween.Sequence();
            mySeq.SetId(cube);
            mySeq.AppendInterval(Random.Range(0, 0.5f));
            mySeq.Append((cube.transform.DOLocalMove(topPos, 0.3f)).SetEase(Ease.OutSine));
            mySeq.Append((cube.transform.DOLocalMove(curPos, 0.3f)).SetEase(Ease.InSine));
            mySeq.SetLoops(1);
        }
    }
}