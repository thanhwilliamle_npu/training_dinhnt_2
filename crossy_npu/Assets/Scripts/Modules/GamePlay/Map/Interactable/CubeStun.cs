﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class CubeStun : MonoBehaviour
{
    public int stunCount;
    [SerializeField] private GameObject locker;
    [SerializeField] private GameObject locker_1;

    private void Awake()
    {
        stunCount = 1;
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.GetComponent<CharacterControl>())
        {
            stunCount = 1;
        }
    }

    private void _MoveLockerToPos(GameObject lockObj, Vector3 pos, float duration)
    {
        DOTween.Kill(lockObj);
        var seq = DOTween.Sequence();
        seq.SetId(lockObj);

        seq.Append(lockObj.transform
            .DOLocalMove(pos, 0.1f)
            .SetEase(Ease.OutQuart)
        );
        seq.AppendInterval(duration);
        seq.Append(lockObj.transform
            .DOLocalMove(Vector3.up * 0.5f, 0.1f)
            .SetEase(Ease.InQuart)
        );
    }
    public void PresentStun(float duration)
    {
        _MoveLockerToPos(locker, Vector3.up * 1.5f, duration);
        _MoveLockerToPos(locker_1, Vector3.up * 2f, duration);
    }
}
