﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class CubeSlide : MonoBehaviour
{
    [SerializeField] public Vector2Int direction;
    [SerializeField] private GameObject arrowNode;
    [SerializeField] private List<GameObject> arrowList;

    private void Awake()
    {
        _DirectArrow();
        // for (int i = 0; i < arrowList.Count; i += 1)
        // {
        //     var arrow = arrowList[i];
        //     _MoveArrowAtIdx(i);
        // }
    }
    
#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        if (!Application.isPlaying) _DirectArrow();
    }

    public void ChangeDirection()
    {
        if (direction.magnitude == 0) direction = Vector2Int.up;
        else
        {
            var oldValue = direction;
            direction.x = -oldValue.y;
            direction.y = oldValue.x;
        }
    }
#endif
    
    private void _DirectArrow()
    {
        if (direction.magnitude == 0)
        {
            arrowNode.transform.localEulerAngles = new Vector3(-90, 0, 0);
            arrowNode.transform.localPosition += Vector3.up * 0.5f;
        }
        else
        {
            var x2D = direction.y;
            var y2D = direction.x;

            var cos = x2D / direction.magnitude;
            var sin = y2D / direction.magnitude;
            var alphaRad = Mathf.Acos(cos);
            var alphaDegree = Mathf.Rad2Deg * alphaRad;

            if (sin < 0) alphaDegree *= -1;
            arrowNode.transform.localEulerAngles = new Vector3(0, alphaDegree, 0);
        }
    }

    private void _MoveArrowAtIdx(int idx)
    {
        var arrow = arrowList[idx];
        var startZ = -0.4f;
        var endZ = 0.2f;
        var delay = 1.5f;

        arrow.transform
            .DOLocalMove(Vector3.forward * startZ, idx * delay)
            .OnComplete(() =>
            {
                var seq = DOTween.Sequence();
                seq.Append(arrow.transform
                    .DOLocalMove(Vector3.forward * endZ, 3)
                    .SetEase(Ease.Linear));
                seq.AppendCallback(() => { arrow.transform.localPosition = Vector3.forward * startZ; });
                seq.SetLoops(-1);
            });
    }
}