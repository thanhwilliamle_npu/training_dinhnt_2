﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEditor;
using UnityEngine;

public class CubeJump : MonoBehaviour
{
    [SerializeField] public Vector2Int direction;
    [SerializeField] private GameObject arrow;
    [SerializeField] private GameObject target;

    private void OnDrawGizmos()
    {
        var center = transform.position + Vector3.up + new Vector3(direction.x, 0, direction.y);
        var a = center + Vector3.forward / 2 + Vector3.left / 2; 
        var b = center + Vector3.forward / 2 + Vector3.right / 2; 
        var c = center + Vector3.back / 2 + Vector3.right / 2; 
        var d = center + Vector3.back / 2 + Vector3.left / 2; 
        Handles.DrawSolidRectangleWithOutline(new [] {a, b, c, d}, Color.cyan, Color.blue);
    }

    private void Awake()
    {
        if (direction.magnitude == 0)
        {
            arrow.transform.localEulerAngles = new Vector3(-90, 0, 0);
            arrow.transform.position += Vector3.up * 0.5f;
        }
        else
        {
            var x2D = direction.y;
            var y2D = direction.x;
        
            var cos = x2D / direction.magnitude;
            var sin = y2D / direction.magnitude;
            var alphaRad = Mathf.Acos(cos);
            var alphaDegree = Mathf.Rad2Deg * alphaRad;

            if (sin < 0) alphaDegree *= -1;
            arrow.transform.localEulerAngles = new Vector3(0, alphaDegree, 0);
        }

        // var seq = DOTween.Sequence();
        // seq.Append(arrow.transform
        //     .DOMove(Vector3.up / 5, 2)
        //     .SetEase(Ease.Linear)
        //     .SetRelative()
        // );
        // seq.Append(arrow.transform
        //     .DOMove(Vector3.down / 5, 2)
        //     .SetEase(Ease.Linear)
        //     .SetRelative()
        // );
        // seq.SetLoops(-1);

        target.transform.localPosition = new Vector3(direction.x, 0, direction.y);
        var seq2 = DOTween.Sequence();
        seq2.Append(target.transform
            .DOScale(Vector3.up + (Vector3.one - Vector3.up) * 1.1f, 1f)
        );
        seq2.Append(target.transform
            .DOScale(Vector3.one, 1f)
        );
        seq2.SetLoops(-1);
    }
}
