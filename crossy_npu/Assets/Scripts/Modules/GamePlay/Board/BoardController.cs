﻿using System;
using UnityEngine;

public class BoardController : SequencialObject
{
    [SerializeField] private GameObject board;
    [SerializeField] private int boardLen;
    [SerializeField] private GameObject prefab;
    
    void Awake()
    {
        // init board presenter
        var lenPerStack = 1;
        var z = (float) (lenPerStack - boardLen) / 2;
        for (int i = 0; i < boardLen; i++)
        {
            var stack = Instantiate(prefab, board.transform);
            stack.transform.localPosition = new Vector3(0, 0, z);
            z += lenPerStack;
        }

        // init collider
        var colliderBoard = board.GetComponent<BoxCollider>();
        colliderBoard.size = new Vector3(0.9f, 0.2f, boardLen);
    }

    protected override GameObject GetGameObject()
    {
        return board;
    }

    public int GetBoardLength()
    {
        return boardLen;
    }

    public float GetBoardSpeed()
    {
        return Speed;
    }

    public float GetNearestGridZPosition(float z)
    {
        float leftMostGrid = - (float) boardLen / 2 + 0.5f;
        float zFromLeftMostGrid = z - leftMostGrid;
        float rounded = Mathf.RoundToInt(zFromLeftMostGrid);
        return leftMostGrid + rounded;
    }
}
