﻿using System;
using DG.Tweening;
using UnityEngine;

public abstract class CharacterMoveStrategy
{
    [NonSerialized] public float moveDuration;
    public abstract void MoveToTarget(Transform transform, Vector3 target);
}

public class StrategyJump : CharacterMoveStrategy
{
    public override void MoveToTarget(Transform transform, Vector3 target)
    {
        transform.DOJump(target, 1.5f, 1, moveDuration, false);
    }
}

public class StrategySlide : CharacterMoveStrategy
{
    public override void MoveToTarget(Transform transform, Vector3 target)
    {
        transform.DOMove(target, moveDuration).SetEase(Ease.Linear);
    }
}

public class CharacterControl : MonoBehaviour
{
    private CharacterMoveStrategy strategy;
    private StrategyJump StrategyJump = new StrategyJump();
    private StrategySlide StrategySlide = new StrategySlide();

    public float GetCurrentMoveTime()
    {
        return strategy.moveDuration;
    }
    public void SetJump(float time)
    {
        strategy = StrategyJump;
        strategy.moveDuration = time;
    }
    public void SetSlide(float time)
    {
        strategy = StrategySlide;
        strategy.moveDuration = time;
    }

    public Vector2Int GetNearestLogical()
    {
        var curPos = transform.position;
        return new Vector2Int(Mathf.RoundToInt(curPos.x), Mathf.RoundToInt(curPos.z));
    }

    public Vector3 RoundToGridLogically(Vector3 pos)
    {
        return new Vector3(Mathf.RoundToInt(pos.x), Mathf.RoundToInt(pos.y), Mathf.RoundToInt(pos.z));
    }
    
    public void InitializeReady()
    {
        transform.rotation = Quaternion.identity;
        _attachedGameObject = null;
    }

    private Vector3 _RotationVectorByDirection(Vector2Int direction)
    {
        if (direction.magnitude == 0) return Vector3.zero;;

        var x2D = direction.y;
        var y2D = direction.x;
        
        var cos = x2D / direction.magnitude;
        var sin = y2D / direction.magnitude;
        var alphaRad = Mathf.Acos(cos);
        var alphaDegree = Mathf.Rad2Deg * alphaRad;

        if (sin < 0) alphaDegree *= -1;
        return new Vector3(0, alphaDegree, 0);
    }

    private void _RotateAndDetach(Vector2Int vector2)
    {
        transform.DORotate(_RotationVectorByDirection(vector2), strategy.moveDuration);
        _DetachRigid();
    }
    
    public void MoveByVector(Vector2Int vector2)
    {
        _RotateAndDetach(vector2);

        Vector3 dir = new Vector3(vector2.x, 0, vector2.y);
        Vector3 target = RoundToGridLogically(transform.position + dir);
        strategy.MoveToTarget(transform, target);
    }
    
    public void JumpToWater(Vector2Int vector2)
    {
        _RotateAndDetach(vector2);

        var componentCollide = GetComponent<SphereCollider>();
        componentCollide.enabled = false;
        Vector3 dir = new Vector3(vector2.x * 1.5f, -2, vector2.y);
        strategy.MoveToTarget(transform, transform.position + dir);
    }
    
    public void JumpToBoard(Vector2Int vector2, BoardController board)
    {
        _RotateAndDetach(vector2);

        var boardTransform = board.transform;
        var boardPosition = boardTransform.position;
        Vector3 currPos = transform.position;
        Vector3 dir = new Vector3(vector2.x, 0, vector2.y);
        
        // calculate position on board if jump immediately
        Vector3 newWorldPos = currPos + dir;
        newWorldPos.y = boardPosition.y + 0.1f;
        var divideLength = (float) board.GetBoardLength() / 2;
        var leftBound = boardPosition.x - divideLength + 0.5f;
        var rightBound = boardPosition.x + divideLength - 0.5f;
        if (newWorldPos.x < leftBound) newWorldPos.x = leftBound;
        if (newWorldPos.x > rightBound) newWorldPos.x = rightBound;
        Vector3 localBoardNewPosition = boardTransform.InverseTransformPoint(newWorldPos);
        Vector3 localBoardNewPositionInGrid = new Vector3(
            localBoardNewPosition.x,
            localBoardNewPosition.y,
            board.GetNearestGridZPosition(localBoardNewPosition.z)
        );
        newWorldPos = boardTransform.TransformPoint(localBoardNewPositionInGrid);
        
        // calculate new interpolated position
        Vector3 offset1 = boardTransform.forward * board.GetBoardSpeed() * strategy.moveDuration;
        Vector3 newInterpolatedPosition = newWorldPos + offset1;
        
        strategy.MoveToTarget(transform, newInterpolatedPosition);
    }

    private GameObject _attachedGameObject;
    private Vector3 _rigidOffset;
    public void AttachRigid(GameObject obj, Vector3 offset)
    {
        _attachedGameObject = obj;
        _rigidOffset = offset;
    }

    private void _DetachRigid()
    {
        _attachedGameObject = null;
    }
    
    private void Update()
    {
        if (_attachedGameObject)
        {
            transform.GetComponent<Rigidbody>().transform.position =
                _attachedGameObject.transform.position + _rigidOffset;
        }
    }
}
