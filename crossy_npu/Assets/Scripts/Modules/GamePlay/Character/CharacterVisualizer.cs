﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;

public class CharacterVisualizer : MonoBehaviour
{
    private GameObject _character3D;
    [SerializeField] private GameObject invisiblePresenter;
    [SerializeField] private GameObject textReverseStatus;

    public void InitializeReady()
    {
        if (_character3D) Destroy(_character3D);
        var skinData = GlobalManager.Instance.SkinManager.CurrentSkinData();
        var prefab = skinData.model3D;
        _character3D = Instantiate(prefab, gameObject.transform);
        _character3D.transform.localScale = Vector3.one;
        _character3D.transform.localPosition = Vector3.zero;
        _character3D.transform.localRotation = Quaternion.identity;
        ShowInvisible(false);
        ShowReversing(false);
    }

    public void DeadByCar()
    {
        _character3D.transform.DOScaleY(0.2f, 0.2f);
    }

    public void ShowInvisible(bool isInvisible)
    {
        invisiblePresenter.SetActive(isInvisible);
    }

    public void ShowReversing(bool isReversing)
    {
        textReverseStatus.SetActive(isReversing);
    }

    private void Update()
    {
        textReverseStatus.transform.eulerAngles = new Vector3(90, -45, 0);
    }
}
