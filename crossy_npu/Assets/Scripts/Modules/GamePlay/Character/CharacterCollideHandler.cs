﻿using System;
using UnityEngine;

public class CharacterCollideHandler : MonoBehaviour
{
    [SerializeField] private CharacterVisualizer visualizer;
    [SerializeField] private Rigidbody rigid;
    [SerializeField] private SphereCollider collide;

    public event Action OnJumpWater;
    public event Action OnCollideVehicle;
    
    public void InitializeReady()
    {
        collide.radius = 0.5f;
        collide.enabled = true;
        rigid.velocity = Vector3.zero;
        rigid.angularVelocity = Vector3.zero;
    }

    private void OnTriggerEnter(Collider collision)
    {
        var mapCube = collision.gameObject.GetComponentInParent<MapCube>();
        if (mapCube && mapCube.type == MapCubeType.Water)
        {
            OnJumpWater?.Invoke();
        }
        
        CarController carController = collision.gameObject.GetComponent<CarController>();
        if (carController)
        {
            OnCollideVehicle?.Invoke();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        BoardController board = collision.gameObject.GetComponent<BoardController>();
        if (board)
        {
            var offset = gameObject.transform.position - board.transform.position;
            gameObject.GetComponent<CharacterControl>().AttachRigid(board.gameObject, offset);
        }
    }
}
