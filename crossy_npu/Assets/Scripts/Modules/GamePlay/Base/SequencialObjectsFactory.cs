﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public abstract class SequencialObject : MonoBehaviour
{
    protected bool Active;
    protected float Speed;

    public bool IsActive()
    {
        return Active;
    }

    public void StartRunObject(float speed)
    {
        var obj = GetGameObject();
        obj.transform.localPosition = Vector3.zero;
        obj.transform.localRotation = Quaternion.identity;
        obj.SetActive(true);
        
        Speed = speed;
        Active = true;
    }
    
    protected abstract GameObject GetGameObject();

    public void StopRunning()
    {
        var obj = GetGameObject();
        obj.SetActive(false);
        Active = false;
    }

    public void RunEachFrame()
    {
        var obj = GetGameObject();
        obj.transform.position += transform.forward * Time.deltaTime * Speed;
        const int sideWayLength = 5;
        if (Math.Abs(obj.transform.localPosition.z) >
            GamePlayManager.Instance.GetMapLength() + sideWayLength * 2) StopRunning();
    }
}

public class SequencialObjectsFactory : MonoBehaviour
{
    [SerializeField] protected float timeToGenNextObj;
    [SerializeField] protected float startDelay;
    [SerializeField] protected float speed;

    [SerializeField] protected SequencialObject prefab;
    [SerializeField] protected int length;
    [SerializeField] private GameObject presenter;
    [SerializeField] private GameObject pathPresenter;
    private SequencialObject[] _list;

    private float _lastTimeGenObj;
    private float _elapsed;

    // Start is called before the first frame update
    void Awake()
    {
        _list = new SequencialObject[length];
        for (int i = 0; i < length; i += 1)
        {
            var obj = Instantiate(prefab, gameObject.transform);
            obj.StopRunning();
            _list[i] = obj;
        }

        _lastTimeGenObj = - timeToGenNextObj;
        _PauseFactory(startDelay);
        presenter.SetActive(false);
        pathPresenter.SetActive(false);
    }

    // public void ShowPath(int maxLength)
    // {
    //     float starterZ = 1.5f;
    //     pathPresenter.transform.localScale = Vector3.one + Vector3.forward * (maxLength - 1);
    //     pathPresenter.transform.localPosition = Vector3.forward * ((float)maxLength / 2 + starterZ);
    // }
    //
    // public Vector3 GetUIPosition()
    // {
    //     return pathPresenter.transform.position - gameObject.transform.forward + Vector3.up * 2;
    // }

    
#if UNITY_EDITOR
    public void Flip(int minLogicalX, int maxLogicalX)
    {
        var currentPosition = gameObject.transform.position;
        if (currentPosition.x <= minLogicalX)
        {
            currentPosition.x = maxLogicalX + 1;
            gameObject.transform.position = currentPosition;
            gameObject.transform.rotation = Quaternion.Euler(0, -90, 0);
        }
        else
        {
            currentPosition.x = minLogicalX - 1;
            gameObject.transform.position = currentPosition;
            gameObject.transform.rotation = Quaternion.Euler(0, 90, 0);
        }
    }
#endif
    
    private float _pauseDuration;

    private void _PauseFactory(float duration)
    {
        _pauseDuration = duration;
    }

    private bool _IsPaused()
    {
        return _pauseDuration > 0;
    }

    private bool _ReachDeadlineGenerate()
    {
        return _elapsed - _lastTimeGenObj > timeToGenNextObj;
    }

    // Update is called once per frame
    void Update()
    {
        if (_IsPaused())
        {
            _pauseDuration -= Time.deltaTime;
            return;
        }
        
        _elapsed += Time.deltaTime;

        foreach (var obj in _list)
        {
            obj.RunEachFrame();
        }

        if (_ReachDeadlineGenerate() && _GenerateObj())
        {
            _lastTimeGenObj = _elapsed;
        }
    }

    private bool _GenerateObj()
    {
        var obj = _GetFreeObj();
        if (obj)
        {
            obj.StartRunObject(speed);
            return true;
        }

        return false;
    }

    private SequencialObject _GetFreeObj()
    {
        foreach (var obj in _list)
        {
            if (!obj.IsActive()) return obj;
        }

        return null;
    }

    public List<SequencialObject> GetActiveObjects()
    {
        return _list.Where((obj) => obj.IsActive()).ToList();
    }

    public void FactoryPause(float duration)
    {
        _PauseFactory(duration);
    }
}