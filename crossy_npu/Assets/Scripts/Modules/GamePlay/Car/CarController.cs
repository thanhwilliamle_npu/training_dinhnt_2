﻿using System;
using UnityEngine;

public class CarController : SequencialObject
{
    [SerializeField] private GameObject car;

    protected override GameObject GetGameObject()
    {
        return car;
    }
}
