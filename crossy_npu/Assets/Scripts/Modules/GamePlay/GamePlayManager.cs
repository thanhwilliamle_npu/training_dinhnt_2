﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class GamePlayManager : MonoBehaviour
{
    [SerializeField] private GameObject characterContainer;
    private CharacterControl _characterControl;
    private CharacterVisualizer _characterVisualizer;
    [SerializeField] private GameObject mapContainer;
    [SerializeField] private GameObject[] mapPrefabs;
    private MapData _mapData;
    [SerializeField] private GameObject cameraContainer;
    [SerializeField] private GameObject marker;

    private bool _playing;
    private float _elapsed;
    private Vector2 _startTouchPos;
    private Vector2 _endTouchPos;
    private const int QueueSize = 4;
    private Queue<Vector2Int> _queue = new Queue<Vector2Int>();
    private Queue<Vector2Int> _slideQueue = new Queue<Vector2Int>(); 
    private float _stampForcedSlide;
    private float _timeSlide = 0.2f;
    private float _stampJumping;
    private float _timeJump = 0.2f;
    private float _stampForcedJumping;
    private float _timeForcedJump = 0.3f;
    private float _maxGapMarker = 4;
    private float _markerSpeed = 0.3f; // pixel/s
    private float _stampStun;
    private float _timeStun = 3;
    private float _timePause = 3;
    private float _stampInvisible;
    private float _timeInvisible = 3;    
    private float _stampReverse;
    private float _timeReverse = 3;
    
    static GamePlayManager _instance;
    public static GamePlayManager Instance => _instance;
    
    private void Awake()
    {
        _instance = this;
        var skinManager = GlobalManager.Instance.SkinManager;
        skinManager.OnChangeSkin += _OnSkinChange;
    }
    
    private void _OnSkinChange()
    {
        _characterVisualizer.InitializeReady();
    }

    public void PrepareNewGame()
    {
        marker.transform.localPosition = new Vector3(0, 0, -3.5f);
        _elapsed = 0;
        _stampJumping = Mathf.NegativeInfinity;
        _stampForcedJumping = Mathf.NegativeInfinity;
        _stampForcedSlide = Mathf.NegativeInfinity;
        _stampStun = Mathf.NegativeInfinity;
        _stampInvisible = Mathf.NegativeInfinity;
        _stampReverse = Mathf.NegativeInfinity;
        _stampReverse = Mathf.NegativeInfinity;

        if (_mapData) Destroy(_mapData.gameObject);
        var levelMgr = GlobalManager.Instance.LevelManager;
        var levelData = levelMgr.LevelDataByCurrentLevel();
        var mapData = Instantiate(levelData.mapPrefab, mapContainer.transform);
        _mapData = mapData.GetComponent<MapData>();
        foreach (var item in _mapData.EdibleItemList)
        {
            item.EatItem += _OnEatItem;
        }

        // init camera
        cameraContainer.transform.position = _mapData.StartPoint.GetStartPosition();
        
        // initialize character
        _InitCharacter();
        
        // init marker pos
        marker.transform.position = _mapData.StartPoint.GetStartPosition() - Vector3.forward * _maxGapMarker;
    }

    private void _InitCharacter()
    {
        _characterControl = characterContainer.GetComponent<CharacterControl>();;
        _characterControl.InitializeReady();
        
        _characterVisualizer = characterContainer.GetComponent<CharacterVisualizer>();
        _characterVisualizer.InitializeReady();
        
        var charCollide = characterContainer.GetComponent<CharacterCollideHandler>();
        charCollide.InitializeReady();

        _characterControl.transform.position = _mapData.StartPoint.GetStartPosition();
        
        // listen to collide event
        charCollide.OnJumpWater -= _OnCharacterJumpWater;
        charCollide.OnJumpWater += _OnCharacterJumpWater;
        
        charCollide.OnCollideVehicle -= _OnCharacterCollideVehicle;
        charCollide.OnCollideVehicle += _OnCharacterCollideVehicle;
    }

    private void _OnCharacterJumpWater()
    {
        if (_playing) GameLose();
    }

    private void _OnCharacterCollideVehicle()
    {
        if (_playing && !_CharacterInvisible())
        {
            _characterVisualizer.DeadByCar();
            GameLose();
        }
    }
    
    public void StartGame()
    {
        _playing = true;
        GuiManager.Instance.ShowGameUI();
    }

    public int GetMapLength()
    {
        return _mapData.MapLength;
    }

    // Update is called once per frame
    void Update()
    {
        if (!_playing) return;
        _elapsed += Time.deltaTime;

        _HandleSlideCharacter();
        _HandleMoveCharacter();
        _HandleMoveMarker();
        _HandleMoveCamera();
        _HandleGameLogic();
        _HandleShowHideCharacterEffect();
    }
    
    private void _HandleSlideCharacter()
    {
        if (_slideQueue.Count == 0) return;
        if (_CharacterBusyMoving()) return;

        var componentController = _characterControl;
        componentController.SetSlide(_timeSlide);
        
        Vector2Int dir = _slideQueue.Dequeue();
        _stampForcedSlide = _elapsed;
        _MoveToTargetByVector(dir);
    }

    private void _HandleMoveCharacter()
    {
        if (_elapsed - _stampForcedJumping < _timeForcedJump) return;
        if (_elapsed - _stampForcedSlide < _timeSlide) return;
        if (_elapsed - _stampStun < _timeStun) return;
        
        if (Input.GetMouseButtonDown(0))
        {
            _startTouchPos = Input.mousePosition;
        }

        if (Input.GetMouseButtonUp(0))
        {
            _endTouchPos = Input.mousePosition;
            if (_queue.Count >= QueueSize) return;
            
            Vector2 directionalVectorLocalCamera = _endTouchPos - _startTouchPos;
            
            if (directionalVectorLocalCamera.magnitude < 40)
            {
                _PushMovementForward();
                return;
            }

            if (_CharacterReverse())
                directionalVectorLocalCamera *= -1;
            
            Vector3 world = cameraContainer.GetComponentInChildren<Camera>().transform.TransformDirection(directionalVectorLocalCamera);
            world.y = 0;
            float angle = Vector3.SignedAngle(Vector3.forward, world, Vector3.down);
            if (45 >= angle && angle > -45)
                _PushMovementForward();
            else if (135 >= angle && angle > 45)
                _PushMovementLeft();
            else if (-45 >= angle && angle > -135)
                _PushMovementRight();
            else 
                _PushMovementBack();
        }

        _MoveCharByJumpFromQueue();
    }
    
    private void _PushMovementForward()
    {
        _queue.Enqueue(Vector2Int.up);
    }    
    private void _PushMovementBack()
    {
        _queue.Enqueue(Vector2Int.down);
    }    
    private void _PushMovementLeft()
    {
        _queue.Enqueue(Vector2Int.left);
    }    
    private void _PushMovementRight()
    {
        _queue.Enqueue(Vector2Int.right);
    }
    
    private void _MoveCharByJumpFromQueue()
    {
        if (_queue.Count == 0) return;
        if (_CharacterBusyMoving()) return;

        var componentController = _characterControl;
        componentController.SetJump(_timeJump);
        
        Vector2Int dir = _queue.Dequeue();
        Vector2Int curLogical = componentController.GetNearestLogical();
        Vector2Int newPos = curLogical + dir;
        if (_mapData.CanJumpIn(newPos))
            _stampJumping = _elapsed;
        _MoveToTargetByVector(dir);
    }

    private void _MoveToTargetByVector(Vector2Int vector2Int)
    { 
        var componentController = _characterControl;
        Vector2Int curLogical = componentController.GetNearestLogical();
        Vector2Int newPos = curLogical + vector2Int;
        if (_mapData.CanJumpIn(newPos))
        {
            if (_mapData.IsWater(newPos))
            {
                if (!_mapData.BoardFactories.ContainsKey(newPos.y))
                {
                    componentController.MoveByVector(vector2Int);
                }
                else
                {
                    var boardFactory = _mapData.BoardFactories[newPos.y];
                    var potentialBoards = boardFactory.GetActiveObjects();
                    Vector3 newPosFloat = componentController.transform.position + Vector3.right * vector2Int.x + Vector3.forward * vector2Int.y;

                    BoardController resultBoard = null;
                    foreach (BoardController board in potentialBoards)
                    {
                        // check if can jump
                        var boardPosition = board.transform.position;
                        var divideLength = (float) board.GetBoardLength() / 2;
                        var leftBound = boardPosition.x - divideLength - board.GetBoardSpeed() * componentController.GetCurrentMoveTime();
                        var rightBound = boardPosition.x + divideLength + board.GetBoardSpeed() * componentController.GetCurrentMoveTime();

                        if (newPosFloat.x < rightBound && newPosFloat.x > leftBound)
                        {
                            resultBoard = board;
                            break;
                        }
                    }

                    if (resultBoard) componentController.JumpToBoard(vector2Int, resultBoard);
                    else
                    {
                        componentController.JumpToWater(vector2Int);
                        _mapData.LogicalCube[newPos].GetComponent<SplashWater>().DoSplash();
                        GameLose();
                    }
                }
            }
            else
            {
                componentController.MoveByVector(vector2Int);
            }
        }
    }
    
    private void _HandleMoveMarker()
    {
        var componentController = _characterControl;
        var gap = componentController.GetNearestLogical().y - marker.transform.position.z;
        if (gap > _maxGapMarker) marker.transform.position += Vector3.forward * (gap - _maxGapMarker);
        marker.transform.position += Vector3.forward * (Time.deltaTime * _markerSpeed);
    }
    
    private void _HandleMoveCamera()
    {
        var charPos = characterContainer.transform.localPosition;
        var targetZ1 = marker.transform.localPosition.z + _maxGapMarker;
        var targetZ2 = charPos.z;
        var z = Mathf.Max(targetZ1, targetZ2);
        var x = charPos.x;
        var target = new Vector3(x, 0, z);
        cameraContainer.transform.localPosition = Vector3.Lerp(cameraContainer.transform.localPosition, target, 0.02f);
    }
    
    private void _HandleGameLogic()
    {
        var componentController = _characterControl;
        var charPos = _characterControl.transform.position;
        var logicalPos = componentController.GetNearestLogical();
        if (charPos.z < marker.transform.position.z) GameLose();
        if (charPos.x <= _mapData.leftBound || charPos.x >= _mapData.rightBound) GameLose();
        
        if (_CharacterBusyMoving()) return;

        var currentCube = _mapData.LogicalCube[logicalPos];
        if (currentCube.type == MapCubeType.FinishLine) GameWin();
        
        if (currentCube.type == MapCubeType.Jump)
        {
            var dir = currentCube.GetComponent<CubeJump>().direction;
            componentController.SetJump(_timeForcedJump);
            _MoveToTargetByVector(dir);
            
            _queue.Clear();
            _stampForcedJumping = _elapsed;
        }
        
        if (currentCube.type == MapCubeType.Slide)
        {
            var dir = currentCube.GetComponent<CubeSlide>().direction;
            _slideQueue.Enqueue(dir);
            _queue.Clear();
        }

        if (currentCube.type == MapCubeType.Stun)
        {
            CubeStun stun = currentCube.GetComponent<CubeStun>();
            if (stun.stunCount > 0)
            {
                stun.stunCount -= 1;
                _stampStun = _elapsed;
                _queue.Clear();
                stun.PresentStun(_timeStun);
            }
        }
    }

    private void _HandleShowHideCharacterEffect()
    {
        _characterVisualizer.ShowInvisible(_CharacterInvisible());
        _characterVisualizer.ShowReversing(_CharacterReverse());
    }
    
    private void _OnEatItem(EdibleItem item)
    {
        if (item.type == EdibleType.StopAlarm)
        {
            foreach (int key in _mapData.BoardFactories.Keys)  
            {  
                _mapData.BoardFactories[key].FactoryPause(_timePause); 
            }
            
            foreach (int key in _mapData.CarFactories.Keys)  
            {  
                _mapData.CarFactories[key].FactoryPause(_timePause); 
            }
        }

        if (item.type == EdibleType.Invisible)
        {
            _stampInvisible = _elapsed;
        }
        
        if (item.type == EdibleType.Reverse)
        {
            _stampReverse = _elapsed;
        }        
        
        if (item.type == EdibleType.Coin)
        {
            UserData.Gold += 1;
        }
    }

    private bool _CharacterBusyMoving()
    {
        var forceJump = _elapsed - _stampForcedJumping < _timeForcedJump;
        var forceSlide = _elapsed - _stampForcedSlide < _timeSlide;
        var normalJump = _elapsed - _stampJumping < _timeJump;
        return forceJump || forceSlide || normalJump;
    }

    private bool _CharacterInvisible()
    {
        return _elapsed - _stampInvisible < _timeInvisible;
    }    
    
    private bool _CharacterReverse()
    {
        return _elapsed - _stampReverse < _timeReverse;
    }
    
    private void _EndGame()
    {
        GuiManager.Instance.DestroyGui<GameUI>();
        _playing = false;
    }
    
    public void GameLose()
    {
        _EndGame();
        GuiManager.Instance.ShowGameLost();
    }

    public void GameWin()
    {
        _EndGame();
        _TriggerOnGameWin();
        
        GuiManager.Instance.ShowGameWon();
    }
    public event Action OnGameWin;
    private void _TriggerOnGameWin()
    {
        OnGameWin?.Invoke();
    }
    
    public void Revive()
    {
        var potentialPos = marker.transform.position + Vector3.forward * _maxGapMarker;
        
        // search for revive position
        Vector2Int potentialVector = new Vector2Int();
        potentialVector.y = Mathf.RoundToInt(potentialPos.z);
        bool found = false;
        while (!found)
        {
            if (!_mapData.PotentialReviveRow(potentialVector.y))
            {
                potentialVector.y -= 1;
                continue;
            }

            int max = Math.Max(Math.Abs(
                _mapData.rightBound - 1), Math.Abs(
                _mapData.leftBound + 1));
            
            for (int i = 0; i <= max; i += 1)
            {
                potentialVector.x = i;
                if (_mapData.CanJumpIn(potentialVector))
                {
                    found = true;
                    break;
                }
                
                potentialVector.x = -i;
                if (_mapData.CanJumpIn(potentialVector))
                {
                    found = true;
                    break;
                }
            }
        }

        Vector3 revivePos = new Vector3(potentialVector.x, 2, potentialVector.y);

        _InitCharacter();
        var rigid = characterContainer.GetComponent<Rigidbody>();
        rigid.position = revivePos;
        rigid.transform.position = revivePos;

        marker.transform.position = revivePos - Vector3.forward * _maxGapMarker;
        StartCoroutine(Delay(0.2f, StartGame));
    }

    IEnumerator Delay(float f, System.Action cb)
    {
        yield return new WaitForSeconds(f);
        cb?.Invoke();
    }
}
