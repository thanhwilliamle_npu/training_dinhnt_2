﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Base.Utils;
using Random = UnityEngine.Random;

[CreateAssetMenu]
public class SkinManager : ScriptableObject
{
    [SerializeField] private List<SkinData> allSkins;
    [SerializeField] public SkinData defaultSkin;
    public List<SkinData> AllSkins => allSkins;

    public void Init()
    {
        GlobalManager.Instance.LevelManager.ClearLevel += _OnLevelClear;
    }

    public Action OnChangeSkin;
    public string CurrentSkinId
    {
        get => PlayerPrefs.GetString("SelectingSkinId", defaultSkin.id);
        set
        {
            PlayerPrefs.SetString("SelectingSkinId", value);
            OnChangeSkin?.Invoke();
        }
    }

    public SkinData CurrentSkinData()
    {
        return SkinDataById(CurrentSkinId);
    }

    public SkinData SkinDataById(string skinId)
    {
        SkinData skin = null;
        foreach (var skinData in AllSkins)
        {
            if (skinId == skinData.id) skin = skinData;
        }
        return skin;
    }

    private void _OnLevelClear()
    {
        LevelManager mgr = GlobalManager.Instance.LevelManager;
        var level = mgr.CurrentLevel;
        foreach (SkinData skinData in AllSkins)
        {
            if (skinData.unlockLevel == level && skinData.unlockMethod == SkinUnlockMethod.COMPLETE_LEVEL)
                skinData.Unlocked = true;
        }
    }

    public bool HaveCharacterNotOwned()
    {
        return AllSkins.Any((sData) => !sData.Owned);
    }

    public List<SkinData> ListCharacterNotOwned()
    {
        return AllSkins.Where((sData) => !sData.Owned).ToList();
    }
}
