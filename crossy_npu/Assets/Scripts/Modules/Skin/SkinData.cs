﻿using System;
using UnityEngine;

public enum SkinUnlockMethod{
    NONE = 0,
    COMPLETE_LEVEL = 1,
    OPEN_CHEST = 2,
}

[CreateAssetMenu(menuName = "Skin/SkinData")]
public class SkinData : ScriptableObject
{
    [SerializeField] public string id = Guid.NewGuid().ToString();
    [SerializeField] public string skinName;
    [SerializeField] public int price;
    [SerializeField] public Sprite icon;
    [SerializeField] public GameObject model3D;
    [SerializeField] public SkinUnlockMethod unlockMethod;
    [SerializeField] public int unlockLevel;

    public bool Unlocked
    {
        get
        {
            if (this == GlobalManager.Instance.SkinManager.defaultSkin) return true;
            return PlayerPrefs.GetInt($"Skin_{id}_unlocked", 0) > 0;
        }
        set => PlayerPrefs.SetInt($"Skin_{id}_unlocked", value ? 1 : 0) ;
    }

    public bool Owned
    {
        get
        {
            if (this == GlobalManager.Instance.SkinManager.defaultSkin) return true;
            return PlayerPrefs.GetInt($"Skin_{id}_owned", 0) > 0;
        }
        set => PlayerPrefs.SetInt($"Skin_{id}_owned", value ? 1 : 0) ;
    }
}