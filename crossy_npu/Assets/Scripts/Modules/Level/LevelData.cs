﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

[CreateAssetMenu(menuName = "Level/LevelData")]
public class LevelData : ScriptableObject
{
    [SerializeField] public GameObject mapPrefab;
    [SerializeField] public int goldScale;

    public int GoldByLevel()
    {
        int minGoldAdd = 5;
        int maxGoldAdd = 15;
        return Mathf.RoundToInt(Random.Range(minGoldAdd + goldScale * 2, maxGoldAdd + goldScale * 2));
    }
}