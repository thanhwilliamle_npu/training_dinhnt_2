﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.PlayerLoop;
using Random = UnityEngine.Random;

[CreateAssetMenu(menuName = "Level/LevelManager")]
public class LevelManager : ScriptableObject
{
    [NonSerialized] public LevelData WishData = null;
    [SerializeField] private List<LevelData> allLevel;
    public List<LevelData> AllLevels => allLevel;

    public void Init()
    {
        GamePlayManager.Instance.OnGameWin += _OnGameWon;
    }

    private void _OnGameWon()
    {
        ClearLevel?.Invoke();
        UserData.Gold += LevelDataByCurrentLevel().GoldByLevel();
        CurrentLevel += 1;
    }

    public Action ClearLevel;
    
    public int CurrentLevel
    {
        get => PlayerPrefs.GetInt("CurrentLevel", 0);
        set => PlayerPrefs.SetInt("CurrentLevel", value);
    }

    public LevelData LevelDataByCurrentLevel()
    {
        if (WishData) return WishData;
        int curLevel = CurrentLevel;
        if (curLevel >= allLevel.Count) return allLevel[(int) Mathf.Floor(Random.Range(0, allLevel.Count))];
        return allLevel[curLevel];
    }

    public string LevelDisplay(int level)
    {
        return (level + 1) + "";
    }
}
