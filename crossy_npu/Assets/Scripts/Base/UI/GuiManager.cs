﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuiManager : MonoBehaviour
{
    [SerializeField] private GameObject screenCanvas;
    [SerializeField] private GameObject sticky;
    
    static GuiManager _instance;
    public static GuiManager Instance => _instance;
    
    private void Awake()
    {
        _instance = this;
    }
    
    void DestroyAllGuis()
    {
        foreach (Transform child in screenCanvas.transform) {
            Destroy(child.gameObject);
        }
    }

    public void DestroyGui<T>()
    {
        T obj = screenCanvas.GetComponentInChildren<T>();
        MonoBehaviour g = obj as MonoBehaviour;
        if (g != null) Destroy(g.gameObject);
    }

    [SerializeField] private GuiLobbyScript lobbyPrefab;
    public void ShowLobby()
    {
        Instantiate(lobbyPrefab, screenCanvas.transform);
    }    
    
    [SerializeField] private GameUI gameUiPrefab;
    public void ShowGameUI()
    {
        Instantiate(gameUiPrefab, screenCanvas.transform);
    }

    [SerializeField] private NodeMoney pnGoldPrefab;
    public void StickPanelGold()
    {
        Instantiate(pnGoldPrefab, sticky.transform);
    }
    
    [SerializeField] private GameLost gameLostPrefab;
    public void ShowGameLost()
    {
        Instantiate(gameLostPrefab, screenCanvas.transform);
    }
    
    [SerializeField] private GameWon gameWonPrefab;
    public void ShowGameWon()
    {
        Instantiate(gameWonPrefab, screenCanvas.transform);
    }    
    
    [SerializeField] private GuiShop guiShopPrefab;
    public void ShowShop()
    {
        Instantiate(guiShopPrefab, screenCanvas.transform);
    }    
    
    [SerializeField] private GameChest gameChestPrefab;
    public void ShowGuiOpenChest()
    {
        Instantiate(gameChestPrefab, screenCanvas.transform);
    }    
    
    [SerializeField] private NotEnoughMoney notEnoughMoneyPrefab;
    public void ShowNotEnoughMoney()
    {
        Instantiate(notEnoughMoneyPrefab, screenCanvas.transform);
    }
}