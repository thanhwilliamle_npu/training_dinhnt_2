﻿using System;
using UnityEngine;


public class GameController : MonoBehaviour
{
    static GameController _instance;
    public static GameController Instance => _instance;
    
    private void Awake()
    {
        _instance = this;
        InitGame();
    }

    void InitGame()
    {
        GlobalManager.Instance.Init();
        
        GuiManager.Instance.ShowLobby();
        GuiManager.Instance.StickPanelGold();
    }
}
