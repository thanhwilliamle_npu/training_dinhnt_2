﻿using UnityEngine;


[CreateAssetMenu]
public class GlobalManager : ScriptableObject
{
    private static GlobalManager _instance;
    public static GlobalManager Instance => _instance ?? (_instance = Resources.Load<GlobalManager>("Mgr/GlobalManager"));

    [SerializeField] SkinManager skinManager;
    public SkinManager SkinManager => skinManager;
    
    [SerializeField] LevelManager levelManager;
    public LevelManager LevelManager => levelManager;    
    
    [SerializeField] ChestMgr chestMgr;
    public ChestMgr ChestMgr => chestMgr;

    public void Init()
    {
        skinManager.Init();
        levelManager.Init();
        chestMgr.Init();
    }
}
