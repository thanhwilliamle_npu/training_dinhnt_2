﻿using System.Collections.Generic;
using UnityEngine;

namespace Base.Utils
{
    public class PlayerPrefsUtils
    {
        public static void SaveListString(string key, List<string> list)
        {
            PlayerPrefs.SetInt("List" + key + "Length", list.Count);
            for (int i = 0; i < list.Count; i += 1)
            {
                PlayerPrefs.SetString("List" + key + "" + i, list[i]);
            }
        }

        public static List<string> GetListString(string key)
        {
            int length = PlayerPrefs.GetInt("List" + key + "Length", 0);
            List<string> list = new List<string>();
            for (int i = 0; i < length; i += 1)
            {
                var str = PlayerPrefs.GetString("List" + key + "" + i, "");
                list[i] = str;
            }

            return list;
        }
    }
}
