﻿public class LevelResources
{
    public static string MapSavePath() => "Assets/ResourcesObjects/Level";
    public static string LevelMgrPath() => "Assets/ResourcesObjects/Level/LevelManager.asset";
}