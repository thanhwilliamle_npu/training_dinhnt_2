﻿using log4net.Core;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(LevelData))]
public class LevelEditor : Editor
{
    public override void OnInspectorGUI()
    {
        var levelData = target as LevelData;
        serializedObject.UpdateIfRequiredOrScript();
        EditorGUILayout.PropertyField(serializedObject.FindProperty("goldScale"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("mapPrefab"));
        serializedObject.ApplyModifiedProperties();
    }
}