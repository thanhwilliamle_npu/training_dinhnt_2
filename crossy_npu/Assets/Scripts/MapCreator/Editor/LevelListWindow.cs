﻿using System;
using UnityEditor;
using UnityEngine;

public class LevelList : EditorWindow
{
    [MenuItem("Window/LevelList")]
    static void Init()
    {
        // Get existing open window or if none, make a new one:
        LevelList window =
            (LevelList) GetWindow(typeof(LevelList));
        window.Show();
    }

    private LevelData _levelData;
    private LevelManager _levelManager;

    private void OnEnable()
    {
        _levelManager = GlobalManager.Instance.LevelManager;
    }

    private void OnGUI()
    {
        _CreateAndLoad();
        _LevelInfo();
        _LevelListInfo();
        Repaint();
    }

    private void _CreateAndLoad()
    {
        EditorGUILayout.Space();
        
        if (GUILayout.Button("Create new Level"))
        {
            string path = EditorUtility.SaveFilePanelInProject(
                "Create new Level",
                "Level",
                "asset",
                "Create new Level",
                LevelResources.MapSavePath());

            if (path.Length > 0)
            {
                _levelData = CreateInstance<LevelData>();
                AssetDatabase.CreateAsset(_levelData, path);
            }
        }
        
        _DrawDivider();
    }

    private void _LevelInfo()
    {
        _levelData = (LevelData) EditorGUILayout.ObjectField("Level", _levelData, typeof(LevelData), false);
        if (!_levelData) return;
        
        var a = Editor.CreateEditor(_levelData);
        a.OnInspectorGUI();

        if (GUILayout.Button("Play Now"))
        {
            _levelManager.WishData = _levelData;
            EditorApplication.ExecuteMenuItem("Edit/Play");
        }
        
        _DrawDivider();
    }

    private void _LevelListInfo()
    {
        var a = Editor.CreateEditor(_levelManager);
        a.OnInspectorGUI();
        _DrawDivider();
    }

    private void _DrawDivider()
    {
        EditorGUILayout.Space();

        var rect = EditorGUILayout.BeginHorizontal();
        Handles.color = Color.gray;
        Handles.DrawLine(new Vector2(rect.x - 15, rect.y), new Vector2(rect.width + 15, rect.y));
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.Space();
        
        EditorGUILayout.Space();
    }
}
