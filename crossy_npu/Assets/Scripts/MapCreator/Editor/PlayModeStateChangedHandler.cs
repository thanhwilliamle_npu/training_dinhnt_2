﻿using UnityEngine;
using UnityEditor;

// ensure class initializer is called whenever scripts recompile
[InitializeOnLoadAttribute]
public static class PlayModeStateChangedHandler
{
    // register an event handler when the class is initialized
    static PlayModeStateChangedHandler()
    {
        EditorApplication.playModeStateChanged -= PlayModeState;
        EditorApplication.playModeStateChanged += PlayModeState;
    }

    private static void PlayModeState(PlayModeStateChange state)
    {
        if (state == PlayModeStateChange.ExitingPlayMode)
            GlobalManager.Instance.LevelManager.WishData = null;
    }
}