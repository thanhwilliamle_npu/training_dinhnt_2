﻿using System;
using System.Collections.Generic;
using System.Linq;
using MapCreator.Editor;
using UnityEditor;
using UnityEditor.Experimental.SceneManagement;
using UnityEditor.SceneManagement;
using UnityEngine;

public class MapEditorWindow : EditorWindow
{
    private GameObject EditingGameObject
    {
        get
        {
            var currPStage = PrefabStageUtility.GetCurrentPrefabStage();
            if (currPStage == null) return null;
            var rootObj = currPStage.prefabContentsRoot;
            if (!rootObj.GetComponent<MapData>()) return null;
            return rootObj;
        }
    }

    private MapData MapData
    {
        get => EditingGameObject ? EditingGameObject.GetComponent<MapData>() : null;
    }

    private enum CubeMode
    {
        Single = 1,
        Line = 2
    }

    private GameObject _selectingPrefab;
    private string _selectingDisplay;
    private CubeMode _cubeMode;
    private string _cubeModeDisplay;

    private GameObject _grassPrefab;
    private GameObject _roadPrefab;
    private GameObject _waterPrefab;
    private GameObject _blockPrefab;
    private GameObject _finishPrefab;
    private GameObject _jumpPrefab;
    private GameObject _slidePrefab;
    private GameObject _stunPrefab;

    private GameObject _carFactoryPrefab;
    private GameObject _boardFactoryPrefab;

    private GameObject _goldPrefab;
    private GameObject _reversePrefab;
    private GameObject _stopAlarmPrefab;
    private GameObject _invisiblePrefab;

    [MenuItem("Window/MapEditor")]
    static void Init()
    {
        // Get existing open window or if none, make a new one:
        MapEditorWindow window = (MapEditorWindow) GetWindow(typeof(MapEditorWindow));
        window.Show();
    }

    private void OnSelectionChange()
    {
        _SaveDataForNextFrame();
    }

    private void OnEnable()
    {
        _selectingDisplay = "NONE";
        _cubeModeDisplay = "Single";
        _cubeMode = CubeMode.Single;

        _grassPrefab = AssetDatabase.LoadAssetAtPath<GameObject>(MapResources.PrefabPathByCubeType(MapCubeType.Grass));
        _roadPrefab = AssetDatabase.LoadAssetAtPath<GameObject>(MapResources.PrefabPathByCubeType(MapCubeType.Road));
        _waterPrefab = AssetDatabase.LoadAssetAtPath<GameObject>(MapResources.PrefabPathByCubeType(MapCubeType.Water));
        _blockPrefab = AssetDatabase.LoadAssetAtPath<GameObject>(MapResources.PrefabPathByCubeType(MapCubeType.Block));
        _finishPrefab =
            AssetDatabase.LoadAssetAtPath<GameObject>(MapResources.PrefabPathByCubeType(MapCubeType.FinishLine));
        _jumpPrefab = AssetDatabase.LoadAssetAtPath<GameObject>(MapResources.PrefabPathByCubeType(MapCubeType.Jump));
        _slidePrefab = AssetDatabase.LoadAssetAtPath<GameObject>(MapResources.PrefabPathByCubeType(MapCubeType.Slide));
        _stunPrefab = AssetDatabase.LoadAssetAtPath<GameObject>(MapResources.PrefabPathByCubeType(MapCubeType.Stun));

        _carFactoryPrefab = AssetDatabase.LoadAssetAtPath<GameObject>(MapResources.CarFactoryPath());
        _boardFactoryPrefab = AssetDatabase.LoadAssetAtPath<GameObject>(MapResources.BoardFactoryPath());

        _goldPrefab = AssetDatabase.LoadAssetAtPath<GameObject>(MapResources.EdibleItemPath(EdibleType.Coin));
        _reversePrefab = AssetDatabase.LoadAssetAtPath<GameObject>(MapResources.EdibleItemPath(EdibleType.Reverse));
        _stopAlarmPrefab = AssetDatabase.LoadAssetAtPath<GameObject>(MapResources.EdibleItemPath(EdibleType.StopAlarm));
        _invisiblePrefab = AssetDatabase.LoadAssetAtPath<GameObject>(MapResources.EdibleItemPath(EdibleType.Invisible));

        autoRepaintOnSceneChange = true;
        SceneView.duringSceneGui += _HandleDuringSceneGui;
    }

    private void _SaveDataForNextFrame()
    {
        if (!EditingGameObject) return;
        MapData.ClearCurrentData();
    }

    private void OnDisable()
    {
        autoRepaintOnSceneChange = true;
        SceneView.duringSceneGui -= _HandleDuringSceneGui;
    }

    void OnGUI()
    {
        _HandleButtonOpenFile();

        if (!EditingGameObject)
        {
            Repaint();
            return;
        }

        var scenePrefab = PrefabStageUtility.GetCurrentPrefabStage().scene;
        var dirty = scenePrefab.isDirty;
        if (dirty) MapData.ClearCurrentData();

        _HandleInfo();
        EditorGUILayout.LabelField("Selecting Object: " + _selectingDisplay);
        _HandleButtonCube();
        _HandleButtonFactory();
        _HandleButtonItem();
        _HandleCubeMode();
        Repaint();
    }

    private void _HandleDuringSceneGui(SceneView sceneView)
    {
        if (!EditingGameObject) return;
        _DrawPlayGround();
        _HandleEvents();
        sceneView.Repaint();
    }

    private void _HandleButtonOpenFile()
    {
        if (GUILayout.Button("Create New Map"))
        {
            var root = PrefabUtility.LoadPrefabContents(MapResources.TemplatePath());
            _SaveMapPrefabAsNew(root);
            _SaveDataForNextFrame();
            PrefabUtility.UnloadPrefabContents(root);
        }

        if (GUILayout.Button("Load Map"))
        {
            _LoadPrefabMap();
            _SaveDataForNextFrame();
        }

        EditorGUILayout.Space();
    }

    private void _HandleInfo()
    {
        GUI.enabled = false;
        EditorGUILayout.ObjectField("Map Prefab", EditingGameObject, typeof(GameObject), true);
        GUI.enabled = true;

        var left = EditorGUILayout.IntField("Left bound", MapData.leftBound);
        if (left != MapData.leftBound)
        {
            MapData.leftBound = left;
            EditorUtility.SetDirty(EditingGameObject);
            _SaveDataForNextFrame();
        }

        var right = EditorGUILayout.IntField("Right bound", MapData.rightBound);
        if (right != MapData.rightBound)
        {
            MapData.rightBound = right;
            EditorUtility.SetDirty(EditingGameObject);
            _SaveDataForNextFrame();
        }

        EditorGUILayout.Space();
    }

    private bool _showCubeBtns;

    private void _HandleButtonCube()
    {
        _showCubeBtns = EditorGUILayout.Foldout(_showCubeBtns, "Cubes");
        if (!_showCubeBtns) return;
        if (GUILayout.Button("Grass"))
        {
            _selectingPrefab = _grassPrefab;
            _selectingDisplay = "Grass";
        }

        if (GUILayout.Button("Road"))
        {
            _selectingPrefab = _roadPrefab;
            _selectingDisplay = "Road";
        }

        if (GUILayout.Button("Water"))
        {
            _selectingPrefab = _waterPrefab;
            _selectingDisplay = "Water";
        }

        if (GUILayout.Button("Blocker"))
        {
            _selectingPrefab = _blockPrefab;
            _selectingDisplay = "Blocker";
        }

        if (GUILayout.Button("Finish Line"))
        {
            _selectingPrefab = _finishPrefab;
            _selectingDisplay = "Finish Line";
        }

        if (GUILayout.Button("Jump"))
        {
            _selectingPrefab = _jumpPrefab;
            _selectingDisplay = "Jump";
        }

        if (GUILayout.Button("Slide"))
        {
            _selectingPrefab = _slidePrefab;
            _selectingDisplay = "Slide";
        }

        if (GUILayout.Button("Stun"))
        {
            _selectingPrefab = _stunPrefab;
            _selectingDisplay = "Stun";
        }

        EditorGUILayout.Space();
    }

    private bool _showFactoryBtns;

    private void _HandleButtonFactory()
    {
        _showFactoryBtns = EditorGUILayout.Foldout(_showFactoryBtns, "Factories");
        if (!_showFactoryBtns) return;
        if (GUILayout.Button("Car"))
        {
            _selectingPrefab = _carFactoryPrefab;
            _selectingDisplay = "Car Factory";
        }

        if (GUILayout.Button("Board"))
        {
            _selectingPrefab = _boardFactoryPrefab;
            _selectingDisplay = "Board Factory";
        }
    }

    private bool _showItemBtns;
    private void _HandleButtonItem()
    {
        _showItemBtns = EditorGUILayout.Foldout(_showItemBtns, "Edible Items");
        if (!_showItemBtns) return;
        if (GUILayout.Button("Gold"))
        {
            _selectingPrefab = _goldPrefab;
            _selectingDisplay = "Gold";
        }

        if (GUILayout.Button("Invisible"))
        {
            _selectingPrefab = _invisiblePrefab;
            _selectingDisplay = "Invisible";
        }        
        
        if (GUILayout.Button("Reverse"))
        {
            _selectingPrefab = _reversePrefab;
            _selectingDisplay = "Reverse";
        }        
        
        if (GUILayout.Button("Stop Alarm"))
        {
            _selectingPrefab = _stopAlarmPrefab;
            _selectingDisplay = "Stop Alarm";
        }
    }

    private void _HandleCubeMode()
    {
        GUILayout.Label("Cube Mode: " + _cubeModeDisplay);
        if (GUILayout.Button("Single"))
        {
            _cubeMode = CubeMode.Single;
            _cubeModeDisplay = "Single";
        }

        if (GUILayout.Button("Line"))
        {
            _cubeMode = CubeMode.Line;
            _cubeModeDisplay = "Line";
        }

        EditorGUILayout.Space();
    }

    private void _OpenSceneWithMap(GameObject map)
    {
        // var scene = EditorSceneManager.OpenScene(MapResources.EditScenePath());
        // var list = scene.GetRootGameObjects();
        // foreach (var obj in list)
        // {
        //     if (obj.GetComponent<MapData>())
        //         DestroyImmediate(obj);
        // }

        AssetDatabase.OpenAsset(map);
    }

    private void _SaveMapPrefabAsNew(GameObject gameObject)
    {
        string path = EditorUtility.SaveFilePanelInProject(
            "Save map as..",
            "NewMap",
            "prefab",
            "Save new level",
            MapResources.MapSavePath());

        if (path.Length > 0)
        {
            var saved = PrefabUtility.SaveAsPrefabAsset(gameObject, path);
            _OpenSceneWithMap(saved);
        }
    }

    private void _LoadPrefabMap()
    {
        string path = EditorUtility.OpenFilePanelWithFilters(
            "Select map to open",
            MapResources.MapSavePath(),
            new[] {".prefab", "prefab"}
        );

        if (path.Length > 0)
        {
            int idx = path.IndexOf(MapResources.MapSavePath());
            path = path.Substring(idx, path.Length - idx);
            var mapGameObject = AssetDatabase.LoadAssetAtPath<GameObject>(path);
            _OpenSceneWithMap(mapGameObject);
        }
    }

    private void _AddObjectAtPositionByMouse()
    {
        var pos = _GetHighestPositionAtMousePos();
        if (_selectingPrefab == _carFactoryPrefab)
        {
            pos.x = MapData.MinLogicalX - 1;
            pos.y = _selectingPrefab.transform.position.y;
            _AddObjectAtPosition(pos, "CarFactory");
        }
        else if (_selectingPrefab == _boardFactoryPrefab)
        {
            pos.x = MapData.MinLogicalX - 1;
            pos.y = _selectingPrefab.transform.position.y;
            _AddObjectAtPosition(pos, "BoardFactory");
        }
        else
        {
            var parentName = "Cubes";
            if (_IsEdibleItems()) parentName = "Items";
            if (_cubeMode == CubeMode.Single)
            {
                _AddObjectAtPosition(pos, parentName);
            }
            else
            {
                for (var i = MapData.MinLogicalX; i <= MapData.MaxLogicalX; i += 1)
                {
                    _AddObjectAtPosition(new Vector3(i, pos.y, pos.z), parentName);
                }
            }
        }
    }

    private void _AddObjectAtPosition(Vector3 pos, string parentName)
    {
        if (!_selectingPrefab) return;
        var original = _selectingPrefab;
        var parent = EditingGameObject.transform.Find(parentName);

        if (parentName == "Cubes")
        {
            pos.y = 0;
            var cubeAtPosition = MapData.GetCubeAtPosition(pos);
            if (cubeAtPosition) Undo.DestroyObjectImmediate(cubeAtPosition.gameObject);
        }        
        
        if (parentName == "Items")
        {
            var cubeAtPosition = MapData.GetCubeAtPosition(pos);
            if (!cubeAtPosition) return;
            
            var itemAtPosition = MapData.GetEdibleItemAtPosition(pos);
            if (itemAtPosition) Undo.DestroyObjectImmediate(itemAtPosition.gameObject);
        }


        var newObj = PrefabUtility.InstantiatePrefab(original, parent) as GameObject;
        if (newObj)
        {
            newObj.transform.localPosition = pos;
            Undo.RegisterCreatedObjectUndo(newObj, "_AddObjectAtPosition" + newObj.GetInstanceID());
        }

        _SaveDataForNextFrame();
        Selection.activeObject = newObj;
    }

    private void _DrawPlayGround()
    {
        var leftMin = new Vector3(MapData.MinLogicalXCanJump - 0.5f, 1, MapData.MinLogicalYCanJump - 0.5f);
        var rightMin = new Vector3(MapData.MaxLogicalXCanJump + 0.5f, 1, MapData.MinLogicalYCanJump - 0.5f);
        var leftMax = new Vector3(MapData.MinLogicalXCanJump - 0.5f, 1, MapData.MaxLogicalYCanJump + 0.5f);
        var rightMax = new Vector3(MapData.MaxLogicalXCanJump + 0.5f, 1, MapData.MaxLogicalYCanJump + 0.5f);

        Handles.DrawLine(leftMin, rightMin);
        Handles.DrawLine(rightMin, rightMax);
        Handles.DrawLine(rightMax, leftMax);
        Handles.DrawLine(leftMax, leftMin);
    }

    private bool _IsFactory()
    {
        return _selectingPrefab == _carFactoryPrefab || _selectingPrefab == _boardFactoryPrefab;
    }
    private bool _IsEdibleItems()
    {
        return
            _selectingPrefab == _stopAlarmPrefab ||
            _selectingPrefab == _reversePrefab ||
            _selectingPrefab == _goldPrefab ||
            _selectingPrefab == _invisiblePrefab;
    }
    
    private void _DrawCubeOnMapByMouse()
    {
        var pos = _GetHighestPositionAtMousePos();
        if (_IsFactory())
        {
            pos.x = MapData.MinLogicalX - 1;
            pos.y = _selectingPrefab.transform.position.y;
            _DrawCubeAtPos(pos);
        }
        else if (_cubeMode == CubeMode.Single)
        {
            _DrawCubeAtPos(pos);
        }
        else
        {
            for (var i = MapData.MinLogicalX; i <= MapData.MaxLogicalX; i += 1)
            {
                _DrawCubeAtPos(new Vector3(i, pos.y, pos.z));
            }
        }
    }

    private void _DrawCubeAtPos(Vector3 pos)
    {
        if (!_selectingPrefab) return;
        var mfs = _selectingPrefab.GetComponentsInChildren<MeshFilter>();
        foreach (var mf in mfs)
        {
            var mr = mf.GetComponent<MeshRenderer>();
            var mt = mf.transform.localToWorldMatrix;
            mt = Matrix4x4.Translate(-_selectingPrefab.transform.position) * mt;
            mt = Matrix4x4.Translate(pos) * mt;
            for (int i = 0; i < mr.sharedMaterial.passCount; i++)
            {
                mr.sharedMaterial.SetPass(i);
                Graphics.DrawMeshNow(mf.sharedMesh, mt);
            }
        }
        
        Handles.DrawWireCube(pos + Vector3.up * 0.5f, Vector3.one);
    }

    private Vector3 _GetPositionOnMapByMouse()
    {
        Vector3 vectorMouse = Event.current.mousePosition;
        vectorMouse.y = Screen.height - vectorMouse.y;
        Ray ray = SceneView.currentDrawingSceneView.camera.ScreenPointToRay(vectorMouse);
        Plane hPlane = new Plane(Vector3.up, Vector3.zero);
        float distance = 0;
        if (hPlane.Raycast(ray, out distance))
        {
            Vector3 pos = ray.GetPoint(distance);
            pos.x = Mathf.RoundToInt(pos.x);
            pos.z = Mathf.RoundToInt(pos.z);
            return pos;
        }

        return Vector3.zero;
    }

    private Vector3 _GetHighestPositionAtMousePos()
    {
        Vector3 vectorMouse = Event.current.mousePosition;
        vectorMouse.y = Screen.height - vectorMouse.y;
        Ray ray = SceneView.currentDrawingSceneView.camera.ScreenPointToRay(vectorMouse);

        var prefabStage = PrefabStageUtility.GetCurrentPrefabStage();
        if (prefabStage != null)
        {
            if (prefabStage.scene.GetPhysicsScene().Raycast(ray.origin, ray.direction, out var hit, queryTriggerInteraction: QueryTriggerInteraction.Ignore))
            {
                var collider = hit.collider;
                
                Vector3 pos = hit.point;
                pos.x = Mathf.RoundToInt(pos.x);
                pos.y = collider.bounds.max.y;
                pos.z = Mathf.RoundToInt(pos.z);
                return pos;
            }
        }
        
        // if (Physics.Raycast(ray, out RaycastHit hitInfo)) return hitInfo.point;
        return _GetPositionOnMapByMouse();
    }

    private void _HandleEvents()
    {
        Event e = Event.current;

        if (e.alt)
        {
            _DrawCubeOnMapByMouse();
            if (e.type == EventType.MouseDown && e.button == 1)
            {
                _AddObjectAtPositionByMouse();
            }
        }

        var target = Selection.activeGameObject;
        if (target && target.GetComponent<SequencialObjectsFactory>() is SequencialObjectsFactory factory)
        {
            var targetPos = factory.transform.position + Vector3.up;
            targetPos.x = (float) (MapData.MaxLogicalX + MapData.MinLogicalX) / 2;
            _ShowGuiOnSceneAtPos(targetPos, new List<string> {"Flip"}, new List<Callback>
            {
                () =>
                {
                    factory.Flip(MapData.MinLogicalX, MapData.MaxLogicalX);
                    EditorUtility.SetDirty(EditingGameObject);
                }
            });
        }

        if (target && target.GetComponent<CubeSlide>() is CubeSlide cubeSlide)
        {
            var targetPos = cubeSlide.transform.position + Vector3.up;
            _ShowGuiOnSceneAtPos(targetPos, new List<string> {"Switch"}, new List<Callback>
            {
                () =>
                {
                    cubeSlide.ChangeDirection();
                    EditorUtility.SetDirty(EditingGameObject);
                }
            });
        }
    }

    private delegate void Callback();

    private void _ShowGuiOnSceneAtPos(Vector3 targetPos, List<string> nameList, List<Callback> cbList)
    {
        var camera = SceneView.currentDrawingSceneView.camera;
        var rectPos = camera.WorldToScreenPoint(targetPos);
        rectPos.y = Screen.height - rectPos.y;
        var size = Vector3.one * 100f;

        Handles.BeginGUI();

        var rectBg = new Rect(rectPos - size / 2, size);
        GUILayout.BeginArea(rectBg, EditorStyles.helpBox);
        for (var index = 0; index < nameList.Count; index++)
            if (GUILayout.Button(nameList[index]))
                cbList[index].Invoke();
        GUILayout.EndArea();

        Handles.EndGUI();
    }
}