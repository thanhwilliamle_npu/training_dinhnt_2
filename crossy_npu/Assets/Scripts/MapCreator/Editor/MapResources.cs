﻿namespace MapCreator.Editor
{
    public class MapResources
    {
        public static string TemplatePath() => "Assets/Contents/Maps/Base/MapTemplate.prefab";

        public static string PrefabPathByCubeType(MapCubeType type)
        {
            switch (type)
            {
                case MapCubeType.Grass:
                    return "Assets/Contents/MapCubes/Grass.prefab";
                case MapCubeType.Road:
                    return "Assets/Contents/MapCubes/Road.prefab";
                case MapCubeType.Water:
                    return "Assets/Contents/MapCubes/Water.prefab";
                case MapCubeType.Block:
                    return "Assets/Contents/MapCubes/Blocker.prefab";
                case MapCubeType.FinishLine:
                    return "Assets/Contents/MapCubes/FinishLine.prefab";
                case MapCubeType.Jump:
                    return "Assets/Contents/MapCubes/Interactable/Jump.prefab";
                case MapCubeType.Slide:
                    return "Assets/Contents/MapCubes/Interactable/Slide.prefab";
                case MapCubeType.Stun:
                    return "Assets/Contents/MapCubes/Interactable/Stun.prefab";
            }

            return "";
        }

        public static string CarFactoryPath() => "Assets/Contents/Cars/CarFactory.prefab";
        public static string BoardFactoryPath() => "Assets/Contents/Boards/BoardFactory.prefab";

        public static string EdibleItemPath(EdibleType type)
        {
            switch (type)
            {
                case EdibleType.Coin:
                    return "Assets/Contents/MapCubes/Item/Gold.prefab";
                case EdibleType.Invisible:
                    return "Assets/Contents/MapCubes/Item/Invisible.prefab";
                case EdibleType.Reverse:
                    return "Assets/Contents/MapCubes/Item/Reverse.prefab";
                case EdibleType.StopAlarm:
                    return "Assets/Contents/MapCubes/Item/StopAlarm.prefab";
            }

            return "";
        }
        public static string EditScenePath() => "Assets/Scenes/MapEditorScene.unity";
        public static string MapSavePath() => "Assets/Contents/Maps";
    }
}